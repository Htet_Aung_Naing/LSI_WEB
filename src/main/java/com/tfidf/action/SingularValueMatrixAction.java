package com.tfidf.action;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.database.util.DataBaseConnection;
import com.mysql.jdbc.Connection;
import com.tfidf.dao.MatrixDao;
import com.tfidf.model.SVUMatrixData;
import com.tfidf.service.SingularValueService;

@ManagedBean(name = "singularValueAction")
@ViewScoped
public class SingularValueMatrixAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1613564845692241590L;
	/**
	 * 
	 */
	
	double [][] singularMatrix ;
	int columncount;
	
	public int getColumncount() {
		return columncount;
	}

	public void setColumncount(int columncount) {
		this.columncount = columncount;
	}

	
	public double[][] getSingularMatrix() {
		return singularMatrix;
	}

	public void setSingularMatrix(double[][] singularMatrix) {
		this.singularMatrix = singularMatrix;
	}

	@PostConstruct
	public void LoadData() 
	{
	
			Connection con = (Connection) DataBaseConnection.getConnection();
			SingularValueService uservice = new SingularValueService();
			
			singularMatrix = uservice.calculateSingularMatrix(con);
			if(singularMatrix != null)
				columncount = singularMatrix[0].length;
				
	}
	
	public String saveData()
	{
		MatrixDao matrixdao = new MatrixDao();
		String res = "";
		ArrayList<SVUMatrixData> singularmatrixList = new ArrayList<>();
		Connection con = (Connection) DataBaseConnection.getConnection();
		if(singularMatrix.length>0)
		{
			for(int i=0; i<singularMatrix.length; i++)
			{
				StringBuilder one_big_row = new StringBuilder();
				SVUMatrixData svumatrix = new SVUMatrixData();
				for(int j=0; j<singularMatrix[i].length; j++)
				{
					one_big_row.append(singularMatrix[i][j]+",");
				}
				svumatrix.setColumndata(one_big_row.toString());
				singularmatrixList.add(svumatrix);
			}
			
			if(matrixdao.insertSVUMatrix(singularmatrixList ,"singular_matrix", con))
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Save Singular Matrix  Successfully"));
			}else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Can't Save!"));
		}
			
		return res;
	}
	
	
	
}
	

