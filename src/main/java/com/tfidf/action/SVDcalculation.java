package com.tfidf.action;

import java.io.IOException;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.view.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.primefaces.model.StreamedContent;

import com.database.util.DataBaseConnection;
import com.mysql.jdbc.Connection;
import com.tfidf.dao.DocumentDao;
import com.tfidf.dao.MatrixDao;
import com.tfidf.model.MatrixAData;
import com.tfidf.model.SVUMatrixData;
import com.tfidf.model.WordCountSearchData;
import com.tfidf.service.EigenVectorMatrixService;
import com.tfidf.service.MatrixCSVexportService;
import com.tfidf.service.SingularValueService;
import com.tfidf.service.UMatrixService;

@ManagedBean(name = "svdcalculation")
@ViewScoped
public class SVDcalculation implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	boolean sbut;
	boolean ubut;
	boolean vbut;
	String previousbut;
	int tvalue;
	
	



	public int getTvalue() {
		return tvalue;
	}



	public void setTvalue(int tvalue) {
		this.tvalue = tvalue;
	}



	@PostConstruct
	public void init()
	{
		//FacesContext facesContext = FacesContext.getCurrentInstance();
		/*HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);*/
		/*previousbut =  (String) session.getAttribute("previousbut");
		this.sbut = false;
		this.ubut = false;
		this.vbut = false;*/
//		if(previousbut != null)
//		{
//			if(previousbut.equals("showmatrix"))
//			{
//				this.sbut = false;
//				this.ubut = true;
//				this.vbut = true;
//			}
//			else if(previousbut.equals("sbut"))
//			{
//				this.sbut = false;
//				this.vbut = false;
//				this.ubut = true;
//			}
//			else if(previousbut.equals("vbut"))
//			{
//				this.sbut = false;
//				this.vbut = false;
//				this.ubut = false;
//			}else
//			{
//				this.sbut = true;
//				this.ubut = true;
//				this.vbut = true;
//			}
//		}else
//		{
//			this.sbut = true;
//			this.ubut = true;
//			this.vbut = true;
//		}
		
	}
	
	

	public boolean isVbut() {
		return vbut;
	}



	public void setVbut(boolean vbut) {
		this.vbut = vbut;
	}



	public boolean isSbut() {
		return sbut;
	}

	public void setSbut(boolean sbut) {
		this.sbut = sbut;
	}

	public boolean isUbut() {
		return ubut;
	}

	public void setUbut(boolean ubut) {
		this.ubut = ubut;
	}
	
	protected static String getSystemPath() {
		String systemPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		return systemPath;
	}
	
	private StreamedContent downLoadFile;

	
	public StreamedContent getDownLoadFile() {
		return downLoadFile;
	}



	public void setDownLoadFile(StreamedContent downLoadFile) {
		this.downLoadFile = downLoadFile;
	}

	public String exportSingularMatrix() throws IOException
	{
		double [][] singularmatrix ;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Connection con = (Connection) DataBaseConnection.getConnection();
		SingularValueService uservice = new SingularValueService();
		
		singularmatrix = uservice.calculateSingularMatrix(con);
		MatrixCSVexportService exportservice = new MatrixCSVexportService();
		exportservice.writeMatrixToResponse(facesContext.getExternalContext(), singularmatrix, "singularMatrix.csv");
		
		return null;
	}
	
	public String exportVectorMatrix() throws IOException
	{
		double [][] eigenvalueMatrix ;
		FacesContext facesContext = FacesContext.getCurrentInstance();
		Connection con = (Connection) DataBaseConnection.getConnection();
		EigenVectorMatrixService vservice = new EigenVectorMatrixService();
		
		eigenvalueMatrix = vservice.caculateVmatrix(con);
		
		MatrixCSVexportService exportservice = new MatrixCSVexportService();
		exportservice.writeMatrixToResponse(facesContext.getExternalContext(), eigenvalueMatrix, "VectorMatrix.csv");
		
		return null;
	}
	
	public String exportUMatrix() throws IOException
	{
		double [][] umatrixexport ;
		Connection con = (Connection) DataBaseConnection.getConnection();
		UMatrixService uservice = new UMatrixService();
		FacesContext facesContext = FacesContext.getCurrentInstance();
		
		umatrixexport = uservice.caculateUmatrix(con);
		MatrixCSVexportService exportservice = new MatrixCSVexportService();
		exportservice.writeMatrixToResponse(facesContext.getExternalContext(), umatrixexport, "UMatrix.csv");
		
		return null;
	}


	public String exportMatrixA() throws IOException
	{
		DocumentDao docDao = new DocumentDao();
		try {
			FacesContext facesContext = FacesContext.getCurrentInstance();
			Connection con = (Connection) DataBaseConnection.getConnection();
			
			columnDocList = docDao.getdynamicHeaderList(con);
			dynamicDataList = docDao.getDynamicWordCountList(new WordCountSearchData() , con);
			MatrixCSVexportService exportservice = new MatrixCSVexportService();
			exportservice.writeExcelToResponse(facesContext.getExternalContext(), columnDocList, dynamicDataList);
			facesContext.responseComplete();
	         facesContext.renderResponse();

		} catch (SQLException e) {
//			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		return null;
	}

	public String showMatrix() 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		session.setAttribute("previousbut", "showmatrix");
	 return "documentMatrixShow";
	}
	
	public String showEigenValueMatrix() 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		session.setAttribute("previousbut", "vbut");
	 return "eigenvalueMatrixShow";
	}
	
	public String calculateUMatrix() 
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		session.setAttribute("previousbut", "ubut");

	 return "umatrixShow";
	}
	public String showSingularValue()
	{
		FacesContext facesContext = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(true);
		session.setAttribute("previousbut", "sbut");

		return "singularmatrixShow";
	}
	
	public String viewallDocument()
	{
		return "documentSearch";
	}
	
	double [][] singularMatrix ;
	public String saveMatrixS()
	{
		Connection con = (Connection) DataBaseConnection.getConnection();
		SingularValueService uservice = new SingularValueService();
		
		singularMatrix = uservice.calculateSingularMatrix(con);
		
		MatrixDao matrixdao = new MatrixDao();
		ArrayList<SVUMatrixData> singularmatrixList = new ArrayList<>();
		if(singularMatrix.length>0)
		{
			for(int i=0; i<singularMatrix.length; i++)
			{
				StringBuilder one_big_row = new StringBuilder();
				SVUMatrixData svumatrix = new SVUMatrixData();
				for(int j=0; j<singularMatrix[i].length; j++)
				{
					one_big_row.append(singularMatrix[i][j]+",");
				}
				svumatrix.setColumndata(one_big_row.toString());
				singularmatrixList.add(svumatrix);
			}
			
			if(matrixdao.insertSVUMatrix(singularmatrixList ,"singular_matrix", con))
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Save Singular Matrix  Successfully"));
			}else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Can't Save!"));
		}
		
		return null;
	}
	
	
	ArrayList<String> columnDocList;
	List<HashMap<String, String>> dynamicDataList;
	
	public String saveMatrixA()
	{
		DocumentDao docDao = new DocumentDao();
		try {
			Connection con = (Connection) DataBaseConnection.getConnection();
			
			columnDocList = docDao.getdynamicHeaderList(con);
			dynamicDataList = docDao.getDynamicWordCountList(new WordCountSearchData() , con);
			
			
			MatrixDao matrixdao = new MatrixDao();
			String res = "";
			ArrayList<MatrixAData> matrixAlist = new ArrayList<>();
			if(columnDocList.size()>0 && dynamicDataList.size()>0)
			{
				for (HashMap<String, String> dynamiccolumn : dynamicDataList) 
				{
					StringBuilder one_big_row = new StringBuilder();
					MatrixAData matrixA = new MatrixAData();
					matrixA.setWordname(dynamiccolumn.get("word"));
					for(int i = 1 ; i<columnDocList.size() ; i++)
					{
						one_big_row.append(dynamiccolumn.get(columnDocList.get(i))+",");
					}
					matrixA.setColumndata(one_big_row.toString());
					matrixAlist.add(matrixA);
				}
				if(matrixdao.insertMatrixA(matrixAlist , con))
				{
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Save Matrix A List Successfully"));
				}else
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Can't Save!"));
			}
				
			return res;
			
		} catch (SQLException e) {
//			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	double [][] eigenvalueMatrix ;

	public String saveMatrixV()
	{
		
		Connection con = (Connection) DataBaseConnection.getConnection();
		EigenVectorMatrixService vservice = new EigenVectorMatrixService();
		
		eigenvalueMatrix = vservice.caculateVmatrix(con);
		
		
		MatrixDao matrixdao = new MatrixDao();
		ArrayList<SVUMatrixData> singularmatrixList = new ArrayList<>();
		if(eigenvalueMatrix.length>0)
		{
			for(int i=0; i<eigenvalueMatrix.length; i++)
			{
				StringBuilder one_big_row = new StringBuilder();
				SVUMatrixData svumatrix = new SVUMatrixData();
				for(int j=0; j<eigenvalueMatrix[i].length; j++)
				{
					one_big_row.append(eigenvalueMatrix[i][j]+",");
				}
				svumatrix.setColumndata(one_big_row.toString());
				singularmatrixList.add(svumatrix);
			}
			
			if(matrixdao.insertSVUMatrix(singularmatrixList ,"v_matrix",con))
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Save Vector Matrix  Successfully"));
			}else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Can't Save!"));
		}
		
		return null;
	}
	
	double [][] umatrix ;
	public String saveMatrixU()
	{
		Connection con = (Connection) DataBaseConnection.getConnection();
		UMatrixService uservice = new UMatrixService();
		
		umatrix = uservice.caculateUmatrix(con);
		
		MatrixDao matrixdao = new MatrixDao();
		ArrayList<SVUMatrixData> singularmatrixList = new ArrayList<>();
		if(umatrix.length>0)
		{
			for(int i=0; i<umatrix.length; i++)
			{
				StringBuilder one_big_row = new StringBuilder();
				SVUMatrixData svumatrix = new SVUMatrixData();
				for(int j=0; j<umatrix[i].length; j++)
				{
					one_big_row.append(umatrix[i][j]+",");
				}
				svumatrix.setColumndata(one_big_row.toString());
				singularmatrixList.add(svumatrix);
			}
			
			if(matrixdao.insertSVUMatrix(singularmatrixList ,"u_matrix", con))
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Save U Matrix  Successfully"));
			}else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Can't Save!"));
		}
		
		
		return null;
	}
	
	public String deleteAllData()
	{
		DocumentDao documentDao = new DocumentDao();

		try {
			if(documentDao.deleteALlData())
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Delete All Data Successfully."));
				return null;
			}else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "INFO!", "Can't Delete."));
				return null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "INFO!", "Can't Delete."));
			return null;
		}
			
	}
	
	public String processData()
	{
		EigenVectorMatrixService vectorMatrixService = new EigenVectorMatrixService();
		if(vectorMatrixService.processTrainedData(tvalue))
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Process Data Successfully."));
			return null;
		}else
		{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "ERROR!", "Can't Process Data!"));
			return null;
		}
				
	}
	
	public String resetVectorValues()
	{
		DocumentDao docDao = new DocumentDao();
		
		try {
			if(docDao.resetVectorValues())
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Reset Vector Values Successfully."));			
			}else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "INFO!", "Can't reset!"));
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	

}
