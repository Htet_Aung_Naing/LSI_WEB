package com.tfidf.action;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.database.util.DataBaseConnection;
import com.mysql.jdbc.Connection;
import com.tfidf.dao.MatrixDao;
import com.tfidf.model.SVUMatrixData;
import com.tfidf.service.UMatrixService;

@ManagedBean(name = "umatrixCalculation")
@ViewScoped
public class UMatrixByDocumentAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1613564845692241590L;
	/**
	 * 
	 */
	
	double [][] umatrix ;
	int columncount;
	
	public int getColumncount() {
		return columncount;
	}

	public void setColumncount(int columncount) {
		this.columncount = columncount;
	}

	public double[][] getUmatrix() {
		return umatrix;
		
	}

	public void setUmatrix(double[][] umatrix) {
		this.umatrix = umatrix;
	}

	@PostConstruct
	public void LoadData() 
	{
	
			Connection con = (Connection) DataBaseConnection.getConnection();
			UMatrixService uservice = new UMatrixService();
			
			umatrix = uservice.caculateUmatrix(con);
			columncount = umatrix[0].length;
				
	}
	
	public String saveData()
	{
		MatrixDao matrixdao = new MatrixDao();
		String res = "";
		ArrayList<SVUMatrixData> singularmatrixList = new ArrayList<>();
		Connection con = (Connection) DataBaseConnection.getConnection();
		if(umatrix.length>0)
		{
			for(int i=0; i<umatrix.length; i++)
			{
				StringBuilder one_big_row = new StringBuilder();
				SVUMatrixData svumatrix = new SVUMatrixData();
				for(int j=0; j<umatrix[i].length; j++)
				{
					one_big_row.append(umatrix[i][j]+",");
				}
				svumatrix.setColumndata(one_big_row.toString());
				singularmatrixList.add(svumatrix);
			}
			
			if(matrixdao.insertSVUMatrix(singularmatrixList ,"u_matrix", con))
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Save U Matrix  Successfully"));
			}else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Can't Save!"));
		}
			
		return res;
	}
	
	
}
	

