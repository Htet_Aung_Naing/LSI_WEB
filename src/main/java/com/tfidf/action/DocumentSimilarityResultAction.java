package com.tfidf.action;

import java.io.Serializable;
import java.util.ArrayList;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.tfidf.model.ComparisonPaperData;


@ManagedBean(name = "documentSimilarityResultAction")
@ViewScoped
public class DocumentSimilarityResultAction implements Serializable {

	/** serialVersionUID */
	private static final long serialVersionUID = 3949168443374966040L;

	ArrayList<ComparisonPaperData> resultList;
	

	
	public ArrayList<ComparisonPaperData> getResultList() {
		return resultList;
	}



	public void setResultList(ArrayList<ComparisonPaperData> resultList) {
		this.resultList = resultList;
	}
	
	



	@SuppressWarnings("unchecked")
	@PostConstruct
	public void init() {
		this.resultList = (ArrayList<ComparisonPaperData>) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("documentSimilarityRanking");
		FacesContext.getCurrentInstance().getExternalContext().getFlash().setKeepMessages(true);
	}
}
