package com.tfidf.action;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;

import com.tfidf.lazymodel.LazyDocumentDataModel;
import com.tfidf.model.DocumentSearchData;

@ManagedBean(name = "docSearchAction")
@ViewScoped
public class DocumentSearchAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6474306882073540913L;
	
	DocumentSearchData docuSearchData;
	LazyDocumentDataModel docuDatamodel;

	
	public LazyDocumentDataModel getDocuDatamodel() {
		return docuDatamodel;
	}

	public void setDocuDatamodel(LazyDocumentDataModel docuDatamodel) {
		this.docuDatamodel = docuDatamodel;
	}

	
	
	public DocumentSearchData getDocuSearchData() {
		return docuSearchData;
	}

	public void setDocuSearchData(DocumentSearchData docuSearchData) {
		this.docuSearchData = docuSearchData;
	}

	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		LoadData();
	}

	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("documentSearchForm:docSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {

			resetPagination(targetDataTable);
			docuDatamodel = new LazyDocumentDataModel(docuSearchData);
		
	}
	
	public void calculateEigenValues()
	{
		
	}
	
	
	
	public void showWordCountDialog()
	{
		
	}
	
	@PostConstruct
	public void LoadData()
	{
		docuSearchData = new DocumentSearchData();
		docuDatamodel = new LazyDocumentDataModel(docuSearchData);
	}
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docId", id);
		return "documentUpdate";
	}


	public String delete(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docId", id);
		return "documentDelete";
	}
	
	public String detail(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docId", id);
		return "documentDetail";
	}
	
	public String wordList(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docId", id);
		return "WordList";
	}
	
	public String changePwd(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docId", id);
		return "passwordChange";
	}
	
	/*public String showMatrix() 
	{
	 return "documentMatrixShow";
	}
	
	public String showEigenValueMatrix() 
	{
	 return "eigenvalueMatrixShow";
	}
	
	public String calculateUMatrix() 
	{
	 return "umatrixShow";
	}
	public String showSingularValue()
	{
		return "singularmatrixShow";
	}*/
	


}
