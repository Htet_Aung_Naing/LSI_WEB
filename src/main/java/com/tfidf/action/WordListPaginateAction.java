package com.tfidf.action;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.component.datatable.DataTable;

import com.tfidf.lazymodel.LazyWordDataModel;
import com.tfidf.model.WordSearchData;

@ManagedBean(name = "wordListAction")
@ViewScoped
public class WordListPaginateAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6474306882073540913L;
	
	WordSearchData wordSearchData;
	LazyWordDataModel wordDataModel;

	
	

	
	


	public WordSearchData getWordSearchData() {
		return wordSearchData;
	}


	public void setWordSearchData(WordSearchData wordSearchData) {
		this.wordSearchData = wordSearchData;
	}


	public LazyWordDataModel getWordDataModel() {
		return wordDataModel;
	}


	public void setWordDataModel(LazyWordDataModel wordDataModel) {
		this.wordDataModel = wordDataModel;
	}


	public void reset(String targetDataTable) {
		resetPagination(targetDataTable);
		LoadData();
	}

	
	private void resetPagination(String targetDataTable) {
		DataTable d = (DataTable) FacesContext.getCurrentInstance().getViewRoot()
				.findComponent("documentSearchForm:docSearchTable");
		d.setFirst(0);
	}
	
	public void search(String targetDataTable) {

		// Check Joined Date From should be earlier than Joined Date To.
			// Reset pagination and do the lazy data model process.
			resetPagination(targetDataTable);
			wordDataModel = new LazyWordDataModel(wordSearchData);
		
	}
	
	@PostConstruct
	public void LoadData()
	{
		String docid = (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("docId");
		wordSearchData = new WordSearchData();
		wordSearchData.setId(Integer.parseInt(docid));
		wordDataModel = new LazyWordDataModel(wordSearchData);
	}
	
	public String edit(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docId", id);
		return "documentUpdate";
	}

	/**
	 * Put the userId as parameter. Go to user delete screen.
	 * 
	 * @param userId
	 * @return action outcome
	 */
	public String delete(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docId", id);
		return "documentUpdate";
	}
	
	public String detail(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docId", id);
		return "UserDetail";
	}
	
	public String changePwd(String id) {
		FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docId", id);
		return "passwordChange";
	}

}
