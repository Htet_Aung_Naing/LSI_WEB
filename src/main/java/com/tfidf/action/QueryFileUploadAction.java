package com.tfidf.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.tfidf.model.DocumentData;
import com.tfidf.service.ExtractRawDataService;

@ManagedBean(name = "tfIdfSaveAction")
@ViewScoped
public class QueryFileUploadAction implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2748502509850168613L;
	public UploadedFile getDocFile() {
		return docFile;
	}

	public void setDocFile(UploadedFile docFile) {
		this.docFile = docFile;
	}

	DocumentData document;
	String serverpath = getSystemPath()+"/document/";
	String docFilepath = "";
	ExtractRawDataService extractService;
	UploadedFile docFile;
	int docId = 0;
	
	protected String getSystemPath() {
		String systemPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		return systemPath;
	}
	
	

	public int getDocId() {
		return docId;
	}

	public void setDocId(int docId) {
		this.docId = docId;
	}

	public DocumentData getDocument() 
	{
		return document;
	}

	public void setDocument(DocumentData document)
	{
		this.document = new DocumentData();
		this.extractService = new ExtractRawDataService();
	}
	
	@PostConstruct
	public void init()
	{
		this.document = new DocumentData();
		this.extractService = new ExtractRawDataService();
		this.docId = extractService.getMaxKey();
	}
	
	public String register()
	{
		
		try {
			String fileName = new Date().toString()+docFile.getFileName().replaceAll("\\s", "_");
			
			long allowSize = 5 * 1024 * 1024;
			long size = docFile.getSize();
			
			if(!docFile.getFileName().equals(""))
			{
				String l_uniquePhotoName = docFile.getFileName().substring(0, docFile.getFileName().lastIndexOf("."));
				l_uniquePhotoName = l_uniquePhotoName+new SimpleDateFormat("yyyyMMdd_HHmm").format(Calendar.getInstance().getTime());;

				// Check file extension.
				String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
				if (!fileExtension.equalsIgnoreCase("pdf")) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File type is not valid!"));
					return null;
				}else
				{
					if (size > allowSize) {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File size is not valid!"));
						return null;
					} else
					{
						boolean flag = false;
						createFile(docFile);
						document.setCode(String.valueOf(docId));
						document.setWords(extractService.extractWords(docFilepath));
						document.setFilepath(l_uniquePhotoName+"."+fileExtension);
						document.setWords(extractService.removeStopWords(document.getWords()));
						
						/*if(extractService.validateDocCode(document.getCode()))
						{*/
						
							flag = extractService.Insert(document);
							if(flag)
							{
								FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Save Successfully"));
								return null;
							}						
							else return null;
						/*}else
						{
							FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR!", "Document Code must not be duplicate "));
							return null;
						}*/
						
						
					}
				}
			}
			else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "File must not be empty."));
				return null;
			}
		
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}
	
	public String showcalculateSVD()
	{
		return "SVDcalculation";
	}
	
	public  void createFile(UploadedFile file) throws IOException {
	
		InputStream in = file.getInputstream();
		String l_uniquePhotoName = docFile.getFileName().substring(0, docFile.getFileName().lastIndexOf("."));
		l_uniquePhotoName = l_uniquePhotoName+new SimpleDateFormat("yyyyMMdd_HHmm").format(Calendar.getInstance().getTime());
		String l_Extension = docFile.getFileName().substring( docFile.getFileName().lastIndexOf(".")+1);
		docFilepath = serverpath+l_uniquePhotoName+"."+l_Extension;
		   OutputStream out = new FileOutputStream(new File(docFilepath));
           
         int read = 0;
         byte[] bytes = new byte[1024];
       
         while ((read = in.read(bytes)) != -1) {
             out.write(bytes, 0, read);
         }
       
         in.close();
         out.flush();
         out.close();
	}
	
	
	
	public ArrayList<String> getExtractWordList(String filePath)
	{
		ArrayList<String> wordList = new ArrayList<String>();
		wordList = extractService.extractWords(filePath);
		return wordList;
	}
	
	public void handleFileUpload(FileUploadEvent event) {
 		UploadedFile uploadedFile = event.getFile();
		String fileName = uploadedFile.getFileName().replaceAll("\\s", "_");
		

		// Check file size.
		long allowSize = 5 * 1024 * 1024;
		long size = uploadedFile.getSize();

		// Check file extension.
		String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
		if (!fileExtension.equalsIgnoreCase("pdf")) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File type is not valid!"));
		}else
		{

		// Check file size.
		if (size > allowSize) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File size is not valid!"));
		} else {

			try {
				InputStream in = uploadedFile.getInputstream();
				   OutputStream out = new FileOutputStream(new File( serverpath+event.getFile().getFileName()));
		              
	                int read = 0;
	                byte[] bytes = new byte[1024];
	              
	                while ((read = in.read(bytes)) != -1) {
	                    out.write(bytes, 0, read);
	                }
	              
	                in.close();
	                out.flush();
	                out.close();
	              
	                System.out.println("New file created!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		}
			
	}
	
	
	
}
