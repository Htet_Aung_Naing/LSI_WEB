package com.tfidf.action;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.tfidf.dao.DocumentDao;
import com.tfidf.model.DocumentData;
import com.tfidf.service.ExtractRawDataService;


@ManagedBean(name = "docDetailAction")
@ViewScoped
public class DocumentDetailAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1932848782212472268L;
	/**
	 * 
	 */
	
	
	String docid;
	DocumentData docData;
	UploadedFile uplodFile;
	String serverpath = getSystemPath()+"/document/";
	private StreamedContent downLoadFile;
	String fileName = "";
	ExtractRawDataService extractService;
	String docFilepath = "";
	DocumentDao docuDao;
		
	public StreamedContent getDownLoadFile() {
		return downLoadFile;
	}

	public void setDownLoadFile(StreamedContent downLoadFile) {
		this.downLoadFile = downLoadFile;
	}



	public String getDocid() {
		return docid;
	}

	public void setDocid(String docid) {
		this.docid = docid;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public DocumentData getDocData() {
		return docData;
	}

	public void setDocData(DocumentData docData) {
		this.docData = docData;
	}

	public UploadedFile getUplodFile() {
		return uplodFile;
	}

	public void setUplodFile(UploadedFile uplodFile) {
		this.uplodFile = uplodFile;
	}

	@PostConstruct
	public void init()
	{
		docuDao = new DocumentDao();
		docData = new DocumentData();
		extractService = new ExtractRawDataService();
		fileName = getSystemPath()+"/document/";
		
		if(docid == null)
		{
			docid =  (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("docId");
			try {
			
				docData = docuDao.getDocumentByParentid(Integer.parseInt(docid));
								       
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally
			{
				
			}
		}
	}
	
	public void prepareDownload()
	{
		InputStream stream = null;
		try {
			if(docData != null)
			{
				stream = new FileInputStream(fileName+docData.getFilepath());
				 downLoadFile = new DefaultStreamedContent(stream, "pdf", new Date().toString()+docData.getFilepath());
			}
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	protected String getSystemPath() {
		String systemPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		return systemPath;
	}

	

}
