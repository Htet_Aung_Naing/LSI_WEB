package com.tfidf.action;

import java.io.Serializable;
import java.sql.SQLException;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.model.StreamedContent;

import com.tfidf.dao.DocumentDao;
import com.tfidf.model.DocumentData;
import com.tfidf.service.ExtractRawDataService;


@ManagedBean(name = "docDeleteAction")
@ViewScoped
public class DocumentDeleteAction implements Serializable{


	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6409729520878289716L;
	String docid;
	DocumentData docData;

	private StreamedContent downLoadFile;
	String fileName = "";
	ExtractRawDataService extractService;
	String docFilepath = "";
	DocumentDao docuDao;
		
	public StreamedContent getDownLoadFile() {
		return downLoadFile;
	}

	public void setDownLoadFile(StreamedContent downLoadFile) {
		this.downLoadFile = downLoadFile;
	}



	public String getDocid() {
		return docid;
	}

	public void setDocid(String docid) {
		this.docid = docid;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public DocumentData getDocData() {
		return docData;
	}

	public void setDocData(DocumentData docData) {
		this.docData = docData;
	}


	@PostConstruct
	public void init()
	{
		docuDao = new DocumentDao();
		docData = new DocumentData();
		extractService = new ExtractRawDataService();
		if(docid == null)
		{
			docid =  (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("docId");
			try {
			
				docData = docuDao.getDocumentByParentid(Integer.parseInt(docid));	
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	
	public String delete()
	{
		DocumentDao documentDao = new DocumentDao();
		try {
			if(documentDao.deleteDocumet(docData.getId()))
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Document Delete Successfully."));
				return null;
			}else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "INFO!", "Can't Delete."));
				return null;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "INFO!", "Can't Delete."));
			return null;
		}
		
	}
	
	
}
