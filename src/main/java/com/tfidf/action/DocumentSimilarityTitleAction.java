package com.tfidf.action;

import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.tfidf.model.ComparisonPaperData;
import com.tfidf.model.DocumentData;
import com.tfidf.model.SetupData;
import com.tfidf.service.ExtractRawDataService;
import com.tfidf.service.QueryCalculationService;

@ManagedBean(name = "documentSimilarityTitleAction")
@ViewScoped
public class DocumentSimilarityTitleAction implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2748502509850168613L;
	
	List<SetupData>documentCountList;
	int docComparisonCount;
	String documentTitle;
	double threadingValue;
	
	public String getDocumentTitle() {
		return documentTitle;
	}

	public void setDocumentTitle(String documentTitle) {
		this.documentTitle = documentTitle;
	}

	public int getDocComparisonCount() {
		return docComparisonCount;
	}

	public void setDocComparisonCount(int docComparisonCount) {
		this.docComparisonCount = docComparisonCount;
	}

	public List<SetupData> getDocumentCountList() {
		return documentCountList;
	}

	public void setDocumentCountList(List<SetupData> documentCountList) {
		this.documentCountList = documentCountList;
	}



	

	public double getThreadingValue() {
		return threadingValue;
	}

	public void setThreadingValue(double threadingValue) {
		this.threadingValue = threadingValue;
	}





	DocumentData document;
	
	
	ExtractRawDataService extractService;

	
	protected String getSystemPath() {
		String systemPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		return systemPath;
	}
	
	

	public DocumentData getDocument() 
	{
		return document;
	}

	public void setDocument(DocumentData document)
	{
		this.document = new DocumentData();
		this.extractService = new ExtractRawDataService();
	}
	
	@PostConstruct
	public void init()
	{
		this.document = new DocumentData();
		this.documentCountList = getDocumentCount();
		this.docComparisonCount = 0;
		extractService = new ExtractRawDataService();
	}
	
	public String calculateSimilarity() throws FileNotFoundException
	{
	
			document.setWords(extractService.extractWordsForTitle(documentTitle));
			document.setWords(extractService.removeStopWords(document.getWords()));
			ArrayList<ComparisonPaperData> compareRatingList = new ArrayList<ComparisonPaperData>();
			QueryCalculationService calculationservice = new QueryCalculationService();
			compareRatingList = calculationservice.getSimilarityResult(document, threadingValue);
			if(compareRatingList.size() > 0)
			{
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("documentSimilarityRanking", compareRatingList);
				return "documentSimilarityResult" ;
			}else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "There are no similar documents!"));
				return null;
			}
			
	}
	
	public List<SetupData> getDocumentCount()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		SetupData set1 = new SetupData();
		set1.setLabel("1");
		set1.setValue(1);
		SetupData set2 = new SetupData();
		set2.setLabel("2");
		set2.setValue(2);
		SetupData set3 = new SetupData();
		set3.setLabel("3");
		set3.setValue(3);
		SetupData set4 = new SetupData();
		set4.setLabel("max");
		set4.setValue(99);
		res.add(set1);
		res.add(set2);
		res.add(set3);
		res.add(set4);
		return res;
	}

	
	
	public ArrayList<String> getExtractWordList(String filePath)
	{
		ArrayList<String> wordList = new ArrayList<String>();
		wordList = extractService.extractWords(filePath);
		
		
		return wordList;
	}
	
	
	
}
