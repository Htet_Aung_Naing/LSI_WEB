package com.tfidf.action;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.sql.SQLException;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.DefaultStreamedContent;
import org.primefaces.model.StreamedContent;
import org.primefaces.model.UploadedFile;

import com.tfidf.dao.DocumentDao;
import com.tfidf.model.DocumentData;
import com.tfidf.service.ExtractRawDataService;


@ManagedBean(name = "docUpdateAction")
@ViewScoped
public class DocumentUpdateAction implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3356441403577889990L;
	
	String docid;
	DocumentData docData;
	UploadedFile uplodFile;
	String serverpath = getSystemPath()+"/document/";
	private StreamedContent downLoadFile;
	String fileName = "";
	ExtractRawDataService extractService;
	String docFilepath = "";
	DocumentDao docuDao;
		
	public StreamedContent getDownLoadFile() {
		return downLoadFile;
	}

	public void setDownLoadFile(StreamedContent downLoadFile) {
		this.downLoadFile = downLoadFile;
	}



	public String getDocid() {
		return docid;
	}

	public void setDocid(String docid) {
		this.docid = docid;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public DocumentData getDocData() {
		return docData;
	}

	public void setDocData(DocumentData docData) {
		this.docData = docData;
	}

	public UploadedFile getUplodFile() {
		return uplodFile;
	}

	public void setUplodFile(UploadedFile uplodFile) {
		this.uplodFile = uplodFile;
	}

	@PostConstruct
	public void init()
	{
		docuDao = new DocumentDao();
		docData = new DocumentData();
		extractService = new ExtractRawDataService();
		fileName = getSystemPath()+"/document/";
		if(docid == null)
		{
			docid =  (String) FacesContext.getCurrentInstance().getExternalContext().getFlash().get("docId");
			try {
			
				docData = docuDao.getDocumentByParentid(Integer.parseInt(docid));
				InputStream stream;
				try {
					stream = new FileInputStream(fileName+docData.getFilepath());
					 downLoadFile = new DefaultStreamedContent(stream, "pdf", new Date().toString()+docData.getFilepath());
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		       
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	protected String getSystemPath() {
		String systemPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		return systemPath;
	}
	
	public String update()
	{
		DocumentDao documentDao = new DocumentDao();
		
		
		
			if(documentDao.update(docData)) 
			{
			
				FacesContext.getCurrentInstance().getExternalContext().getFlash().put("docUpdateData", docData);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Document Update Successfully"));
				return null;
			}
				
			else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Cannot Update!"));
				return null;
			}
				
			
	}
	
	public  void createFile(UploadedFile file) throws IOException {
		
		InputStream in = file.getInputstream();
		String l_uniquePhotoName = uplodFile.getFileName().substring(0, uplodFile.getFileName().lastIndexOf("."));
		l_uniquePhotoName = l_uniquePhotoName+new Date().getTime();
		String l_Extension = uplodFile.getFileName().substring( uplodFile.getFileName().lastIndexOf(".")+1);
		docFilepath = serverpath+l_uniquePhotoName+"."+l_Extension;
		   OutputStream out = new FileOutputStream(new File(docFilepath));
           
         int read = 0;
         byte[] bytes = new byte[1024];
       
         while ((read = in.read(bytes)) != -1) {
             out.write(bytes, 0, read);
         }
       
         in.close();
         out.flush();
         out.close();
	}
	
	public void handleFileUpload(FileUploadEvent event) {
 		UploadedFile uploadedFile = event.getFile();
		String fileName = uploadedFile.getFileName().replaceAll("\\s", "_");
		

		// Check file size.
		long allowSize = 8 * 1024 * 1024;
		long size = uploadedFile.getSize();

		// Check file extension.
		String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
		if (!fileExtension.equalsIgnoreCase("pdf")) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File type is not valid!"));
		}else
		{

		// Check file size.
		if (size > allowSize) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File size is not valid!"));
		} else {

			try {
				InputStream in = uploadedFile.getInputstream();
				   OutputStream out = new FileOutputStream(new File( serverpath+event.getFile().getFileName()));
		              
	                int read = 0;
	                byte[] bytes = new byte[1024];
	              
	                while ((read = in.read(bytes)) != -1) {
	                    out.write(bytes, 0, read);
	                }
	              
	                in.close();
	                out.flush();
	                out.close();
	              
	                System.out.println("New file created!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		}
		
	}
	

}
