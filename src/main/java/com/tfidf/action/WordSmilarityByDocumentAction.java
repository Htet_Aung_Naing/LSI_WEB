package com.tfidf.action;

import java.io.Serializable;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import com.database.util.DataBaseConnection;
import com.mysql.jdbc.Connection;
import com.tfidf.dao.DocumentDao;
import com.tfidf.dao.MatrixDao;
import com.tfidf.lazymodel.LazyDocumentMatrixDataModel;
import com.tfidf.model.DocumentMatrixData;
import com.tfidf.model.MatrixAData;
import com.tfidf.model.SameWordKeyValue;
import com.tfidf.model.WordCountSearchData;

@ManagedBean(name = "wordCountSimilarityAction")
@ViewScoped
public class WordSmilarityByDocumentAction implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1613564845692241590L;
	/**
	 * 
	 */
	ArrayList<DocumentMatrixData> similarWordMatrixList;
	LazyDocumentMatrixDataModel similarWordDataModel;
	ArrayList<String> columnDocList;
	List<HashMap<String, String>> dynamicDataList;

	ArrayList<SameWordKeyValue> wordKeyValueList;

	ArrayList<ArrayList<DocumentMatrixData>> finalResult;
	
	
	public List<HashMap<String, String>> getDynamicDataList() {
		return dynamicDataList;
	}

	public void setDynamicDataList(List<HashMap<String, String>> dynamicDataList) {
		this.dynamicDataList = dynamicDataList;
	}

	public ArrayList<ArrayList<DocumentMatrixData>> getFinalResult() {
		return finalResult;
	}

	public void setFinalResult(ArrayList<ArrayList<DocumentMatrixData>> finalResult) {
		this.finalResult = finalResult;
	}

	public ArrayList<SameWordKeyValue> getWordKeyValueList() {
		return wordKeyValueList;
	}

	public void setWordKeyValueList(ArrayList<SameWordKeyValue> wordKeyValueList) {
		this.wordKeyValueList = wordKeyValueList;
	}
	
	public ArrayList<String> getColumnDocList() {
		return columnDocList;
	}

	public void setColumnDocList(ArrayList<String> columnDocList) {
		this.columnDocList = columnDocList;
	}

	public WordCountSearchData getWordSearchCountData() {
		return wordSearchCountData;
	}

	public void setWordSearchCountData(WordCountSearchData wordSearchCountData) {
		this.wordSearchCountData = wordSearchCountData;
	}

	WordCountSearchData wordSearchCountData = new WordCountSearchData();

	public LazyDocumentMatrixDataModel getSimilarWordDataModel() {
		return similarWordDataModel;
	}

	public void setSimilarWordDataModel(LazyDocumentMatrixDataModel similarWordDataModel) {
		this.similarWordDataModel = similarWordDataModel;
	}

	public ArrayList<DocumentMatrixData> getSimilarWordMatrixList() {
		return similarWordMatrixList;
	}

	public void setSimilarWordMatrixList(ArrayList<DocumentMatrixData> similarWordMatrixList) {
		this.similarWordMatrixList = similarWordMatrixList;
	}

	@PostConstruct
	public void LoadData() 
	{
		DocumentDao docDao = new DocumentDao();
		try {
			Connection con = (Connection) DataBaseConnection.getConnection();
			
			columnDocList = docDao.getdynamicHeaderList(con);
			dynamicDataList = docDao.getDynamicWordCountList(new WordCountSearchData() , con);
		} catch (SQLException e) {
//			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	protected static String getSystemPath() {
		String systemPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		return systemPath;
	}

	
	public String saveData()
	{
		MatrixDao matrixdao = new MatrixDao();
		String res = "";
		ArrayList<MatrixAData> matrixAlist = new ArrayList<>();
		Connection con = (Connection) DataBaseConnection.getConnection();
		if(columnDocList.size()>0 && dynamicDataList.size()>0)
		{
			for (HashMap<String, String> dynamiccolumn : dynamicDataList) 
			{
				StringBuilder one_big_row = new StringBuilder();
				MatrixAData matrixA = new MatrixAData();
				matrixA.setWordname(dynamiccolumn.get("word"));
				for(int i = 1 ; i<columnDocList.size() ; i++)
				{
					one_big_row.append(dynamiccolumn.get(columnDocList.get(i))+",");
				}
				matrixA.setColumndata(one_big_row.toString());
				matrixAlist.add(matrixA);
			}
			if(matrixdao.insertMatrixA(matrixAlist , con))
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "INFO!", "Save Matrix A List Successfully"));
			}else
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "Can't Save!"));
		}
			
		return res;
	}
	
	}
	

