package com.tfidf.action;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

import com.tfidf.model.DocumentData;
import com.tfidf.model.SetupData;
import com.tfidf.service.ExtractRawDataService;

@ManagedBean(name = "documentSimilarityAction")
@ViewScoped
public class DocumentSimilarityAction implements Serializable
{
	/**
	 * 
	 */
	private static final long serialVersionUID = -2748502509850168613L;
	
	List<SetupData>documentCountList;
	int docComparisonCount;
	
	public int getDocComparisonCount() {
		return docComparisonCount;
	}

	public void setDocComparisonCount(int docComparisonCount) {
		this.docComparisonCount = docComparisonCount;
	}

	public List<SetupData> getDocumentCountList() {
		return documentCountList;
	}

	public void setDocumentCountList(List<SetupData> documentCountList) {
		this.documentCountList = documentCountList;
	}

	public UploadedFile getDocFile() {
		return docFile;
	}

	public void setDocFile(UploadedFile docFile) {
		this.docFile = docFile;
	}

	public double getThreadingValue() {
		return threadingValue;
	}

	public void setThreadingValue(double threadingValue) {
		this.threadingValue = threadingValue;
	}

	DocumentData document;
	String serverpath = getSystemPath()+"/similarity/";
	String docFilepath = "";
	double threadingValue;
	ExtractRawDataService extractService;
	UploadedFile docFile;
	
	protected String getSystemPath() {
		String systemPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		return systemPath;
	}
	
	

	public DocumentData getDocument() 
	{
		return document;
	}

	public void setDocument(DocumentData document)
	{
		this.document = new DocumentData();
		this.extractService = new ExtractRawDataService();
	}
	
	@PostConstruct
	public void init()
	{
		this.document = new DocumentData();
		this.documentCountList = getDocumentCount();
		this.docComparisonCount = 0;
		extractService = new ExtractRawDataService();
	}
	
	public String calculateSimilarity()
	{
		try {
			String fileName = new Date().toString()+docFile.getFileName().replaceAll("\\s", "_");
			
			long allowSize = 8 * 1024 * 1024;
			long size = docFile.getSize();
			
			if(!docFile.getFileName().equals(""))
			{
				String l_uniquePhotoName = docFile.getFileName().substring(0, docFile.getFileName().lastIndexOf("."));
				l_uniquePhotoName = l_uniquePhotoName+new Date().getTime();

				// Check file extension.
				String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
				if (!fileExtension.equalsIgnoreCase("pdf")) {
					FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File type is not valid!"));
					return null;
				}else
				{
					if (size > allowSize) {
						FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File size is not valid!"));
						return null;
					} else
					{
						createFile(docFile);
						document.setWords(extractService.extractWords(docFilepath));
						document.setFilepath(l_uniquePhotoName+"."+fileExtension);
						document.setWords(extractService.removeStopWords(document.getWords()));
						
						
						return "documentSimilarityResult" ;
						
					}
				}
			}else
			{
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "ERROR!", "File Name is not valid!"));
				return null;
			}
					
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
		
		

	}
	
	public List<SetupData> getDocumentCount()
	{
		List<SetupData>res = new ArrayList<SetupData>() ;
		SetupData set1 = new SetupData();
		set1.setLabel("1");
		set1.setValue(1);
		SetupData set2 = new SetupData();
		set2.setLabel("2");
		set2.setValue(2);
		SetupData set3 = new SetupData();
		set3.setLabel("3");
		set3.setValue(3);
		SetupData set4 = new SetupData();
		set4.setLabel("max");
		set4.setValue(99);
		res.add(set1);
		res.add(set2);
		res.add(set3);
		return res;
	}
	
	public  void createFile(UploadedFile file) throws IOException {
	
		InputStream in = file.getInputstream();
		String l_uniquePhotoName = docFile.getFileName().substring(0, docFile.getFileName().lastIndexOf("."));
		l_uniquePhotoName = l_uniquePhotoName+new Date().getTime();
		String l_Extension = docFile.getFileName().substring( docFile.getFileName().lastIndexOf(".")+1);
		docFilepath = serverpath+l_uniquePhotoName+"."+l_Extension;
		   OutputStream out = new FileOutputStream(new File(docFilepath));
           
         int read = 0;
         byte[] bytes = new byte[1024];
       
         while ((read = in.read(bytes)) != -1) {
             out.write(bytes, 0, read);
         }
       
         in.close();
         out.flush();
         out.close();
	}
	
	
	
	public ArrayList<String> getExtractWordList(String filePath)
	{
		ArrayList<String> wordList = new ArrayList<String>();
		wordList = extractService.extractWords(filePath);
		return wordList;
	}
	
	public void handleFileUpload(FileUploadEvent event) {
 		UploadedFile uploadedFile = event.getFile();
		String fileName = uploadedFile.getFileName().replaceAll("\\s", "_");
		

		// Check file size.
		long allowSize = 8 * 1024 * 1024;
		long size = uploadedFile.getSize();

		// Check file extension.
		String fileExtension = fileName.substring(fileName.lastIndexOf(".") + 1, fileName.length());
		if (!fileExtension.equalsIgnoreCase("pdf")) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File type is not valid!"));
		}else
		{

		// Check file size.
		if (size > allowSize) {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, "ERROR!", "File size is not valid!"));
		} else {

			try {
				InputStream in = uploadedFile.getInputstream();
				   OutputStream out = new FileOutputStream(new File( serverpath+event.getFile().getFileName()));
		              
	                int read = 0;
	                byte[] bytes = new byte[1024];
	              
	                while ((read = in.read(bytes)) != -1) {
	                    out.write(bytes, 0, read);
	                }
	              
	                in.close();
	                out.flush();
	                out.close();
	              
	                System.out.println("New file created!");
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		}
		
	}
	
}
