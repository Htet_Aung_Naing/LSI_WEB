package com.tfidf.lazymodel;

import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.tfidf.dao.DocumentDao;
import com.tfidf.model.DocumentData;
import com.tfidf.model.DocumentSearchData;
import com.tfidf.model.DocumentSearchPaginateData;

public class LazyDocumentDataModel extends LazyDataModel<DocumentData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8429001188797238226L;
	
	DocumentSearchData docserchData;

	public DocumentSearchData getDocumentSearchData() {
		return docserchData;
	}

	public void setDocumentSearchData(DocumentSearchData DocumentSearchData) {
		this.docserchData = DocumentSearchData;
	}
	
	public  LazyDocumentDataModel(DocumentSearchData usearch) {
		this.docserchData = usearch;
	}

	@Override
	public List<DocumentData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		
		docserchData.setOffset(first);
		docserchData.setLimit(pageSize);
		DocumentDao documentDao = new DocumentDao();
		
		DocumentSearchPaginateData userPaginateData = documentDao.find(docserchData);
		
		setRowCount(userPaginateData.getCount());
		setPageSize(docserchData.getLimit());
		
		List<DocumentData> userList = userPaginateData.getDocumentDataList();
		
		return userList;
	}

}
