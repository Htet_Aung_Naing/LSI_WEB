package com.tfidf.lazymodel;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.tfidf.dao.DocumentDao;
import com.tfidf.model.WordData;
import com.tfidf.model.WordPaginateData;
import com.tfidf.model.WordSearchData;

public class LazyWordDataModel extends LazyDataModel<WordData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8429001188797238226L;
	
	WordSearchData wordSearch;

	
	
	public WordSearchData getWordSearch() {
		return wordSearch;
	}

	public void setWordSearch(WordSearchData wordSearch) {
		this.wordSearch = wordSearch;
	}

	public  LazyWordDataModel(WordSearchData usearch) {
		this.wordSearch = usearch;
	}

	@Override
	public List<WordData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		
		wordSearch.setOffset(first);
		wordSearch.setLimit(pageSize);
		DocumentDao documentDao = new DocumentDao();
		List<WordData> userList = null;
		
		WordPaginateData wordPaginate;
		try {
			wordPaginate = documentDao.getWordDataListbyparenId(wordSearch);
			setRowCount(wordPaginate.getCount());
			setPageSize(wordSearch.getLimit());
			
			userList = wordPaginate.getWordList();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		return userList;
	}

}
