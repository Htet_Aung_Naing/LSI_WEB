package com.tfidf.lazymodel;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;

import com.database.util.DataBaseConnection;
import com.tfidf.dao.DocumentDao;
import com.tfidf.model.DocumentMatrixData;
import com.tfidf.model.SimilarityWordCountPaginateData;
import com.tfidf.model.WordCountSearchData;

public class LazyDocumentMatrixDataModel extends LazyDataModel<DocumentMatrixData>{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8429001188797238226L;
	
	ArrayList<String> documentList;
	
	WordCountSearchData wordCountserchData;

	
	
	public  LazyDocumentMatrixDataModel(WordCountSearchData usearch) {
		this.wordCountserchData = usearch;
	}

	@Override
	public List<DocumentMatrixData> load(int first, int pageSize, String sortField,
			SortOrder sortOrder, Map<String, Object> filters) {
		
		wordCountserchData.setOffset(first);
		wordCountserchData.setLimit(pageSize);
		DocumentDao documentDao = new DocumentDao();
		List<DocumentMatrixData> similarWordList = null;
		SimilarityWordCountPaginateData wordCountPaginateData;
//		try {
//			Connection con = DataBaseConnection.getConnection();
//			wordCountPaginateData = documentDao.getSimilarWordList(wordCountserchData,con);
//			setRowCount(wordCountPaginateData.getCount());
//			setPageSize(wordCountserchData.getLimit());
//			similarWordList = new ArrayList<>();
//			similarWordList = wordCountPaginateData.getSimilarWordList();
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		
	
		
		return similarWordList;
	}

}
