package com.tfidf.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.database.util.DataBaseConnection;
import com.tfidf.dao.DocumentDao;
import com.tfidf.dao.ProcessValueDao;
import com.tfidf.dao.SVUDao;
import com.tfidf.model.DocumentData;
import com.tfidf.model.ProcessValue;
import com.tfidf.model.WordCountSearchData;

import Jama.Matrix;
import Jama.SingularValueDecomposition;

public class EigenVectorMatrixService {
	
	public  double[][] caculateVmatrix(Connection con)
	{
		DecimalFormat df = new DecimalFormat("#.0000"); 
		double [][] result = null;

	try {
			
			DocumentDao docDao = new DocumentDao();
			ArrayList<String> distinctWordList = new ArrayList<>();
			distinctWordList = docDao.getDistinctWordList(new WordCountSearchData() , con);
			ArrayList<DocumentData> docHeaderList = docDao.getDocumentList(con);
			double [][] matrixA = new double[distinctWordList.size()][docHeaderList.size()];
			
			//Matrix A = Matrix.identity(5, 5);
			if(distinctWordList.size() != 0 )
			{			
				for (int r=0; r < matrixA.length; r++) {
					
						 for (int c=0; c<matrixA[r].length; c++) {				    	
								int count = docDao.getsimilarWordCountbyDocument(docHeaderList.get(c).getId() , distinctWordList.get(r),con);
								 matrixA[r][c] = count;
							
				     }
				 }
			}
			
			Matrix A = new Matrix(matrixA);
			
            //A = A.transpose().times(A);
            SingularValueDecomposition S = A.svd();
			
			Matrix V = S.getV();
	
		
		for (int r=0; r < V.getRowDimension(); r++) {
			
			 for (int c=0; c<V.getColumnDimension(); c++) {
	    	
				V.set(r, c, Double.parseDouble((df.format(V.get(r, c)))));
				
	     }
	 }
		
		result = V.getArray();	
		
		
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return result;
	}
	
	public boolean processTrainedData(int threashowValue)
	{
		boolean flag = false;
		SVUDao vectorDao = new  SVUDao();
		DocumentDao docDao = new DocumentDao();
		ProcessValueDao processValueDao = new ProcessValueDao();
		Matrix vectormatrix;
		ArrayList<DocumentData> doclist = new ArrayList<>();
		Connection con = DataBaseConnection.getConnection();
		doclist = docDao.getAllData(con);
		double[][] vmatrix = vectorDao.getSVUMatrix("V_MATRIX",con);
		if(vmatrix.length>0)
		{
			vectormatrix = new Matrix(vmatrix);
			vectormatrix = vectormatrix.transpose();
			for(int column = 0; column<vectormatrix.getColumnDimension(); column++)
	        {
            	processValueDao = new ProcessValueDao();
            	ArrayList<ProcessValue> processValueList = new ArrayList<>();
			for(int row = 0; row<threashowValue ; row++)
	            {
	            	ProcessValue processvalue = new ProcessValue();
	            	processvalue.setProcess_value(vectormatrix.get(row,column));
	            	processValueList.add(processvalue);
	            }
	           processValueDao.insertprocessList(processValueList, doclist.get(column).getId(),con);
	        }
			flag = true;
		}
		
		return flag;
	}
}
