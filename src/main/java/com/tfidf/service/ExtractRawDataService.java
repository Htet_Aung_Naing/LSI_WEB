package com.tfidf.service;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.faces.context.FacesContext;

import org.tartarus.snowball.ext.PorterStemmer;

import com.database.util.DataBaseConnection;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.parser.PdfTextExtractor;
import com.tfidf.dao.DocumentDao;
import com.tfidf.model.DocumentData;
import com.tfidf.model.WordData;

public class ExtractRawDataService {

	static DecimalFormat df = new DecimalFormat("#.0000000"); 
	static DecimalFormat df2 = new DecimalFormat("#.00"); 
	
	public  ArrayList<String> extractWords(String path)
	{
		ArrayList<String> res = new ArrayList<String>();
	    ArrayList<String> cleanlist = new ArrayList<>();
		
		
		PdfReader reader;
		try {
			reader = new PdfReader(path);
			  int n = reader.getNumberOfPages();

			    String myLine = "";
			    // Go through all pages
			    for (int i = 1; i <= n; i++)
			    {
			      // only page number 2 will be included
			     myLine += PdfTextExtractor.getTextFromPage(reader, i);    
			    }
			    String regex = "[0-9][-+.^:,?()\"\'@!]";
			    
			    if(!myLine.equals(""))
			    {		
			    	myLine = myLine.toLowerCase();
			    	 Pattern pattern = Pattern.compile(regex);
			    	    Matcher matcher = pattern.matcher(myLine);
			    	    myLine = matcher.replaceAll("");
			    	res = getPorterStemmer(tokenizeWithSpace(myLine));
			    	
			    	  String regex1 = "[\\-\\+\\.\\;\\^:,]";
					    String regex2 = "[^\\w\\s]";
					    
					    if(res.size()>0)
					    {
					    	
					    	for (String word : res) 
					    	{
					    		if(word.length()>1)
					    		{
					    			word = word.replaceAll(regex1, "");
									word = word.replaceAll(regex2, "");
									cleanlist.add(word);
					    		}
								
							}
					    }
			    }
					    
			    return cleanlist;
			    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	  
	     return cleanlist;
	}
	
	public  ArrayList<String> extractWordsForTitle(String docTitle)
	{
		ArrayList<String> res = new ArrayList<String>();
	    ArrayList<String> cleanlist = new ArrayList<>();
		String regex = "[0-9][-+.^:,?()\"\'@]";
			    
			    if(!docTitle.equals(""))
			    {	
			    	docTitle = docTitle.toLowerCase();
			    	 Pattern pattern = Pattern.compile(regex);
			    	    Matcher matcher = pattern.matcher(docTitle);
			    	    docTitle = matcher.replaceAll("");
			    	    
			    	res = getPorterStemmer(tokenizeWithSpace(docTitle));
			    	
			    	 String regex1 = "[\\-\\+\\.\\;\\^:,]";
					    String regex2 = "[^\\w\\s]";
					    if(res.size()>0)
					    {
					    	
					    	for (String word : res) 
					    	{
					    		if(word.length()>1)
					    		{
					    			word = word.replaceAll(regex1, "");
									word = word.replaceAll(regex2, "");
									cleanlist.add(word);
					    		}
								
							}
					    }
			    	
			    }
			  
		
	     return cleanlist;
	}
	
	public  ArrayList<String> extractWordsWithString(String myLine)
	{
		ArrayList<String> res = new ArrayList<String>();
		String regex = "[0-9][-+.^:,?()\"\'@]";
				    
			    if(!myLine.equals(""))
			    {			   
			    	 Pattern pattern = Pattern.compile(regex);
			    	    Matcher matcher = pattern.matcher(myLine);
			    	    myLine = matcher.replaceAll("");
			    	    
			    	res = getPorterStemmer(tokenizeWithSpace(myLine));
			    	
			    }
			    return res;

	}
	
	
	
	public static ArrayList<String> tokenizeWithSpace(String rawData)
	{
		ArrayList<String> res = new ArrayList<String>();
		rawData=rawData.trim().replaceAll("\\s+", " ");
		String[] words = rawData.split(" ");
		
		for (String word : words) 
		{
			res.add(word);
		}
		
		return res;
	}
	
	public static ArrayList<WordData> getTfScore(ArrayList<String>worList)
	{
		
		ArrayList<WordData> res = new ArrayList<WordData>();
		ArrayList<String>uniqueData = new ArrayList<String>();
		uniqueData = getUniqueData(worList);
		for (String udata : uniqueData) 
		{
			WordData word = new WordData();
			word.setWord(udata);
			word.setCount(getCount(worList, udata));
			res.add(word);
		}
		
		return res;
	}
	
	public static int getCount(ArrayList<String> orgwordlist, String term)
	{
		int count = 0;
		for (String word : orgwordlist) 
		{
		   if (term.equalsIgnoreCase(word))
		       count++;
		}
		
		return count;
	}
	
	public static double tf(List<String> doc, String term , WordData wordData,int maxPerCount) {
	    double result = 0;
	    for (String word : doc) {
	       if (term.equalsIgnoreCase(word))
	              result++;
	       }
	    wordData.setCount((int) result);
	    return result / maxPerCount;
	}
	
	public static double tfTitle(List<String> doc, String term , WordData wordData,int maxPerCount) {
	    double result = 0;
	    for (String word : doc) {
	       if (term.equalsIgnoreCase(word))
	              result++;
	       }
	    wordData.setCount((int) result);
	    return (0.5*result / maxPerCount);
	}
	
	
	public static ArrayList<String> getUniqueData(ArrayList<String>wordList)
	{
		ArrayList<String> result = new ArrayList<String>();
		Set<String> hs = new HashSet<>();
		hs.addAll(wordList);
		result.addAll(hs);
		return result;
	}

	public  ArrayList<String> removeStopWords(ArrayList<String> wordsList)
	{		
		
//				for (int j = 0; j < DocumentData.stopWordsList.length; j++)
//				{
//					if(DocumentData.stopWordsList[j].equals("a"))
//					if (wordsList.contains(DocumentData.stopWordsList[j])) 
//					{
//							wordsList.remove(DocumentData.stopWordsList[j]);
//					}
//				}
		List<String> stopwordlist = Arrays.asList(DocumentData.stopWordsList);
		for(int j = 0; j < wordsList.size(); j++)
		{
			//if(wordsList.get(j).equals("a"))
			if(stopwordlist.contains(wordsList.get(j)))
					{
						wordsList.remove(j);
						j--;
					}
		}
		
		
		return wordsList;
	}
	
	public static ArrayList<String> getPorterStemmer(ArrayList<String>wordList)
	{
		ArrayList<String> res = new ArrayList<String> ();
		
		for (String word : wordList) 
		{
			 PorterStemmer porter = new PorterStemmer();
		      porter.setCurrent(word);
		      porter.stem();
		      res.add(porter.getCurrent());
		}
		
		return res;
	}
	
	public static ArrayList<WordData> prepareSaveData(ArrayList<String> inputdoc) throws SQLException
	{	
		ArrayList<WordData> res = new ArrayList<WordData>();
		res =  getTfScore(inputdoc);
	
		return res;
	}
	

	
	
	public  void getTFIDfWeight(DocumentData document) throws SQLException
	{
		document.setWordList(prepareSaveData(document.getWords()));
	}
	
	public boolean validateDocCode(String code)
	{
		DocumentDao docDao = new DocumentDao();
		return docDao.viladateDocID(code);
	}
	
	public  boolean Insert(DocumentData document) throws SQLException
	{
		boolean flag = false;
		document.setWordList(prepareSaveData(document.getWords()));
		DocumentDao docDao = new DocumentDao();
		flag = docDao.insert(document);
		return flag;
	}

	public int getMaxKey()
	{
		int res = 0;
		DocumentDao docDao = new DocumentDao();
		res = docDao.getMaxDocumentkey();
	
		return res+1;
	}
	
	public  boolean update(DocumentData document) throws SQLException
	{
		boolean flag = false;
		DocumentDao docDao = new DocumentDao();
		flag = docDao.update(document);
		return flag;
	}
	
	
	
	public static int getMaxWordCountAtUserQuery(ArrayList<String>userQuery)
	{
		int max = 0 ;
		Map<String,Integer> histogram = new HashMap<>();
		for (String x : userQuery) { 
			  if (!histogram.containsKey(x)) histogram.put(x,1); 
			  else histogram.put(x,histogram.get(x) + 1);
			}
		for (int x : histogram.values()) max = max > x ? max : x;
		System.out.println("max count is "+max);
		return max;
	}
	
	public static ArrayList<DocumentData> getCountwithWordList() throws SQLException
	{
		ArrayList<DocumentData> docList = new ArrayList<DocumentData>();
		Connection con = DataBaseConnection.getConnection();
		DocumentDao docDao = new DocumentDao();
		docList = docDao.getDocumentList(con);
		{
			for (DocumentData documentData : docList) 
			documentData.setWordList(docDao.getWordDataListbyparenId(documentData.getId(),con));
		}
		return docList;
	}
	
	
	
	
	public static double getsquareUserWordWeight(ArrayList<WordData> wordList)
	{
		double ans = 0 ;
		for (WordData wordData : wordList)
		{
			if(wordData.getWeight()>0)
			{
				ans +=  Double.parseDouble(df2.format(wordData.getWeight())) *  Double.parseDouble(df2.format(wordData.getWeight())) ;
				ans = Double.parseDouble(df2.format(ans));
			}
			
		}
		return ans;
	}
	
	protected static String getSystemPath() {
		String systemPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		return systemPath;
	}

	
	
	
	public static void main(String[]args)
	{
		ArrayList<String> wordList = new ArrayList<String>();
		wordList.add("[");
		wordList.add("a");
		wordList.add("5");
		wordList.add("2");
		ExtractRawDataService service = new ExtractRawDataService();
		service.removeStopWords(wordList);
		System.out.print("");
	}
	
}
