package com.tfidf.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import javax.faces.context.FacesContext;

import org.primefaces.model.DefaultStreamedContent;

import com.database.util.ComperatorClass;
import com.database.util.DataBaseConnection;
import com.tfidf.dao.DocumentDao;
import com.tfidf.dao.SVUDao;
import com.tfidf.model.ComparisonPaperData;
import com.tfidf.model.DocumentData;
import com.tfidf.model.WordCountSearchData;

import Jama.Matrix;


public class QueryCalculationService {
    static DecimalFormat df = new DecimalFormat("#.0000");
	
	public ArrayList<ComparisonPaperData> getSimilarityResult(DocumentData document,double threadshowvalue) throws FileNotFoundException
	{
		ArrayList<ComparisonPaperData> reslist = new ArrayList<>();
		ArrayList<String>distinctWordlist = new ArrayList<>();
		ArrayList<DocumentData> doclist = new ArrayList<>();
		DocumentDao documentDao = new DocumentDao();
		Connection con = DataBaseConnection.getConnection();
		int tvalue = 0;
		
		double [][] res;
		try {
			doclist = documentDao.getAllDatawithProcessValue(con);
			tvalue = doclist.get(0).getProcessValuelsit().size();
			SVUDao svudao = new SVUDao();
			Matrix U = new Matrix(svudao.getSVUMatrixWithconstCount("U_MATRIX",con, tvalue));
			Matrix S = new Matrix(svudao.getSVUMatrix("SINGULAR_MATRIX",con));
			S = getdatabyConstForS(S, tvalue);
			S = S.inverse();
			
			U = getdatabyConstForU(U, tvalue);
			distinctWordlist = documentDao.getDistinctWordList(new WordCountSearchData(), con);
			res = new double[distinctWordlist.size()][1];
			
			for(int i=0; i<distinctWordlist.size(); i++)
			{
				for(int j=0; j<document.getWords().size(); j++)
				{
					
					if(distinctWordlist.get(i).equalsIgnoreCase(document.getWords().get(j)))
					{
						res[i][0] += 1;
					}
				}
			}
			if(res.length != 0)
			{
				
				Matrix q = new Matrix(res);
				 q = q.transpose().times(U);
                 q = q.times(S);
                 
                 reslist = calculateSamilarity(q , doclist ,threadshowvalue);
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return reslist;
	}
	
	public int getCountBysameword(String word , ArrayList<String> wordList)
	{
		int res = 0 ;
		for (String wd : wordList) 
		{
			if(word.equalsIgnoreCase(wd))
				res+=1;
		}
		
		return res;
	}
	
	public Matrix getdatabyConstForU(Matrix m,int constcount)
	{
		Matrix res = new Matrix(m.getRowDimension(),constcount);
		for(int i = 0; i<m.getRowDimension(); i++)
		{
			for(int j=0; j<constcount; j++)
			{
				res.set(i, j, m.get(i, j));
			}
		}
		 return res;
	}
	
	public Matrix getdatabyConstForS(Matrix m,int constcount)
	{
		Matrix res = new Matrix(constcount,constcount);
		for(int i = 0; i<constcount; i++)
		{
			for(int j=0; j<constcount; j++)
			{
				res.set(i, j, m.get(i, j));
			}
		}
		 return res;
	}
	 	
	protected static String getSystemPath() {
		String systemPath = FacesContext.getCurrentInstance().getExternalContext().getRealPath("/");
		return systemPath;
	}

	
	public static ArrayList<ComparisonPaperData> calculateSamilarity(Matrix query, ArrayList<DocumentData> doculist , double constvalue) throws FileNotFoundException
    {
       
        double totalnumerator = 0;
        double totalsquare = 0;
        double qsquare = 0;
        double tcosinesquareroot = 0;
        double tqcosinesquareroot = 0;
   
        ArrayList<ComparisonPaperData> rankinlist = new ArrayList<>();
    	String fileName = getSystemPath()+"/document/";


        for(int j = 0; j<query.getColumnDimension(); j++)
        {
            qsquare  += Math.pow(query.get(0,j),2);
        }
        tqcosinesquareroot = Math.sqrt(qsquare);

        for (DocumentData app:doculist)
        {
        	InputStream stream;
        	File file = new File(fileName+app.getFilepath());
        	if(file.exists())
        		stream = new FileInputStream(fileName+app.getFilepath());
        	else stream = null;
            ComparisonPaperData rank = new ComparisonPaperData();
            totalnumerator = 0;
            totalsquare = 0;
            tcosinesquareroot = 0;
            for(int i=0; i<app.getProcessValuelsit().size(); i++)
            {
                totalnumerator += Double.valueOf(df.format(query.get(0,i) * app.getProcessValuelsit().get(i).getProcess_value()));
              //  totalsquare += Double.valueOf(df.format(Math.pow(query.get(0,i),2) + Math.pow(app.getVectorTransporseValue().get(i),2)));
                totalsquare += Double.valueOf(df.format(Math.pow(app.getProcessValuelsit().get(i).getProcess_value(),2)));

            }
            tcosinesquareroot = Math.sqrt(totalsquare);
            tcosinesquareroot = tcosinesquareroot * tqcosinesquareroot;
            totalsquare = totalsquare + qsquare;
       
            rank.setCode(app.getCode());
            rank.setAuthorName(app.getAuthorName());
            rank.setLabel(app.getName());
			rank.setDownLoadFile( new DefaultStreamedContent(stream, "pdf", new Date().toString()+app.getFilepath()));
           // rank.setSimilarityValue(similarity);
			if(tcosinesquareroot != 0)
			{
				rank.setSimilarityValue(Double.valueOf(df.format(totalnumerator/tcosinesquareroot)));
				if(constvalue > 0)
	            {
	            	if(constvalue <= rank.getSimilarityValue())
	                	rankinlist.add(rank);
	            }else
	            {
	            	rankinlist.add(rank);
	            }

			}
            
            	
           // finalresult = similarity +":"+app.getApp_name();
            //res.add(finalresult);
        }
        Collections.sort(rankinlist , new ComperatorClass());
      
        return  rankinlist;
    }
	
	

}
