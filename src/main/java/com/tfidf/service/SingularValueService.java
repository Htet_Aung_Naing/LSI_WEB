package com.tfidf.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;

import com.tfidf.dao.DocumentDao;
import com.tfidf.model.DocumentData;
import com.tfidf.model.WordCountSearchData;

import Jama.Matrix;
import Jama.SingularValueDecomposition;

public class SingularValueService {
	public  double[][] calculateSingularMatrix(Connection con)
	{
		DecimalFormat df = new DecimalFormat("#.0000"); 
		double [][] result = null;

	try {
			
			DocumentDao docDao = new DocumentDao();
			ArrayList<String> distinctWordList = new ArrayList<>();
			distinctWordList = docDao.getDistinctWordList(new WordCountSearchData() , con);
			ArrayList<DocumentData> docHeaderList = docDao.getDocumentList(con);
			double [][] matrixA = new double[distinctWordList.size()][docHeaderList.size()];
			
			//Matrix A = Matrix.identity(5, 5);
			if(distinctWordList.size() != 0 )
			{			
				for (int r=0; r < matrixA.length; r++) {
					
						 for (int c=0; c<matrixA[r].length; c++) {
				    	
								int count = docDao.getsimilarWordCountbyDocument(docHeaderList.get(c).getId() , distinctWordList.get(r),con);
								 matrixA[r][c] = count;
							
				     }
				 }
			}
			
			if(matrixA.length>0)
			{
				Matrix A = new Matrix(matrixA);
				
				SingularValueDecomposition S = A.svd();
				
				Matrix singularMatrix = S.getS();
				
				for (int r=0; r < singularMatrix.getRowDimension(); r++) {
					
					if(r==99)
					 for (int c=0; c<singularMatrix.getColumnDimension(); c++) {
						 if(c==99)
							 singularMatrix .set(r, c, Double.parseDouble((df.format(singularMatrix.get(r, c)))));
						
			     }
			 }
			
			result = singularMatrix.getArray();	
			}
			
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	return result;
				
		
	}

}
