package com.tfidf.service;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import javax.faces.context.ExternalContext;

import org.primefaces.util.Constants;

import com.opencsv.CSVWriter;

public class MatrixCSVexportService {
	
//	public static void exportCSV(ExternalContext context,List<String> headerlist,List<String> columnlist) throws IOException
//	{
//        CSVWriter writer = new CSVWriter(new FileWriter("MatrixA.csv"));
//		
//		HSSFCellStyle styleHeader = (HSSFCellStyle) workbook.createCellStyle();
//        HSSFFont fontHeader = (HSSFFont) workbook.createFont();
//        fontHeader.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
//        styleHeader.setFont(fontHeader);
//		
//		HSSFSheet sheet = workbook.createSheet("sheet 1");
//		HSSFRow row = sheet.createRow(0); 
//		createHeader(row, activityList,styleHeader);
//		addContent(list, sheet);
//		writeExcelToResponse(context, workbook, "schoolList"+new Date().getTime());
//	
//	}
	 DecimalFormat df = new DecimalFormat("#.0000");
	 public void writeMatrixToResponse(ExternalContext externalContext,double[][]matrix, String filename) throws IOException {
		 OutputStream out = externalContext.getResponseOutputStream();
	     Writer writer = new OutputStreamWriter(out);
	     CSVWriter csvwriter = new CSVWriter(writer);
	     List<String[]> data = new ArrayList<String[]>();
	     
	     if(matrix.length > 0)
	     {
	    	 for (int i = 0; i < matrix.length; i++) 
	         {
	             String[] row = new String[matrix[i].length];
	             for (int j = 0; j < matrix[i].length; j++)
	             {
	                 row[j] = String.valueOf(df.format(matrix[i][j]));
	             }
	             data.add(row);
	         }
	     } 
	    	externalContext.setResponseContentType("text/csv");
	    	externalContext.setResponseHeader("Content-Disposition", "attachment; filename="+filename);
	    	externalContext.addResponseCookie(Constants.DOWNLOAD_COOKIE, "true", Collections.<String, Object>emptyMap());
	    	csvwriter.writeAll(data);
	    	

	    	csvwriter.close();
	    	
	        out.close();
	        externalContext.responseFlushBuffer();        	       
	       
	    }
	
	
	 public void writeExcelToResponse(ExternalContext externalContext,ArrayList<String> columnDocList,List<HashMap<String, String>> dynamicDataList) throws IOException {
		 OutputStream out = externalContext.getResponseOutputStream();
	     Writer writer = new OutputStreamWriter(out);
	     CSVWriter csvwriter = new CSVWriter(writer);

		 String[] headerrow = new String[columnDocList.size()];
	     List<String[]> data = new ArrayList<String[]>();

		 for (int i=0; i<columnDocList.size(); i++)
		 {
			 headerrow[i] = columnDocList.get(i);
		 }
		 data.add(headerrow);
		 
		 for (int i=0; i<dynamicDataList.size(); i++) 
		 {
			 String[] row = new String[columnDocList.size()];
			for (int j = 0; j<columnDocList.size(); j++) 
			{
				row[j] = dynamicDataList.get(i).get(columnDocList.get(j));
			}
			data.add(row);
		 }
		 
		 
		 	
	    	externalContext.setResponseContentType("text/csv");
	    	externalContext.setResponseHeader("Content-Disposition", "attachment; filename=\"MatrixA.csv\"");
	    	externalContext.addResponseCookie(Constants.DOWNLOAD_COOKIE, "true", Collections.<String, Object>emptyMap());
	    	csvwriter.writeAll(data);
	    	
//	    	for (String[] strings : data) {
//				for(int i=0; i<strings.length; i++)
//				{
//					writer.write(strings[i]);
//				}
//			}
	    	csvwriter.close();
	    	
	        out.close();
	        externalContext.responseFlushBuffer();        	       
	       
	    }

	    public static String getContentDisposition(String filename) {
	        return "attachment;filename="+ filename + ".xls";
	    }

}
