package com.tfidf.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tfidf.model.MatrixData;

public class SVUDao {
	
	public double[][] getSVUMatrix(String tablename,Connection con)
	{
		double[][] res = null;
		String query = "Select * from "+tablename;
		try {
				Statement st = con.createStatement();
				 ResultSet rs = st.executeQuery(query);
				 String row = "";
				 String[] eachrow = null;
				 
				/* if(rs != null)
				 {
					 rs.beforeFirst();  
					 rs.last();  
				 

					 row = rs.getString("COLUMN_DATA");
					 eachrow = row.split(",");
					 res = new double[ rs.getRow()][eachrow.length] ;
					
				 }*/
				 
				 ArrayList<MatrixData> matrixlist = new ArrayList<>();
				 while(rs.next())
				 {	
					 MatrixData matrixdata = new MatrixData();
					 row = rs.getString("COLUMN_DATA");
					 eachrow = row.split(",");
					 for(int j=0; j<eachrow.length; j++)
					 {
						 matrixdata.getColumnlist().add(Double.parseDouble(eachrow[j])) ;
						 //res[i][j] = Double.parseDouble(eachrow[j]);
					 }
					 matrixlist.add(matrixdata);
				 }
				 if(matrixlist.size()>0)
				 {
					 res = new double[matrixlist.size()][matrixlist.get(0).getColumnlist().size()];
					 for(int i=0; i<matrixlist.size(); i++)
					 {
						 for(int j=0; j<matrixlist.get(0).getColumnlist().size(); j++)
						 {
							 res[i][j] = matrixlist.get(i).getColumnlist().get(j);
						 }
					 } 
				 }
				
				 
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		
		
		return res;
	}
	
	public double[][] getSVUMatrixWithconstCount(String tablename,Connection con, int count)
	{
		double[][] res = null;
		String query = "Select * from "+tablename;
		try {
				Statement st = con.createStatement();
				 ResultSet rs = st.executeQuery(query);
				 String row = "";
				 String[] eachrow = null;
				 
				/* if(rs != null)
				 {
					 rs.beforeFirst();  
					 rs.last();  
				 

					 row = rs.getString("COLUMN_DATA");
					 eachrow = row.split(",");
					 res = new double[ rs.getRow()][eachrow.length] ;
					
				 }*/
				 
				 ArrayList<MatrixData> matrixlist = new ArrayList<>();
				 while(rs.next())
				 {	
					 MatrixData matrixdata = new MatrixData();
					 row = rs.getString("COLUMN_DATA");
					 eachrow = row.split(",");
					 for(int j=0; j<eachrow.length; j++)
					 {
						 matrixdata.getColumnlist().add(Double.parseDouble(eachrow[j])) ;
						 //res[i][j] = Double.parseDouble(eachrow[j]);
					 }
					 matrixlist.add(matrixdata);
				 }
				 if(matrixlist.size()>0)
				 {
					 res = new double[matrixlist.size()][matrixlist.get(0).getColumnlist().size()];
					 for(int i=0; i<matrixlist.size(); i++)
					 {
						 for(int j=0; j<count; j++)
						 {
							 res[i][j] = matrixlist.get(i).getColumnlist().get(j);
						 }
					 } 
				 }
				
				 
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} 
		
		
		return res;
	}


}
