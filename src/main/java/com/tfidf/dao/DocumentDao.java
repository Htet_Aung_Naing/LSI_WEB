package com.tfidf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.database.util.DataBaseConnection;
import com.tfidf.model.DocumentData;
import com.tfidf.model.DocumentSearchData;
import com.tfidf.model.DocumentSearchPaginateData;
import com.tfidf.model.WordCountSearchData;
import com.tfidf.model.WordData;
import com.tfidf.model.WordPaginateData;
import com.tfidf.model.WordSearchData;

public class DocumentDao {
	
	public  boolean insert(DocumentData doc) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		
		try {
			con = DataBaseConnection.getConnection();
			     
			query = " insert into DOCUMENT (DOC_CODE,AUTHOR_NAME, DOCUMENT_NAME,DESCRIPTION,FILEPATH ) values (?,?,?,?,?)";
			    	        
			    int i = 1;
			    int parentid = 0;
				ps = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
				ps.setString(i++, doc.getCode());
				ps.setString(i++, doc.getAuthorName());
				ps.setString(i++, doc.getName());
				ps.setString(i++, doc.getDescription());
				ps.setString(i++, doc.getFilepath());
				
				ps.execute();
				ResultSet rs = ps.getGeneratedKeys();
				if(rs.next())
					parentid = rs.getInt(1);
				insertWordList(doc.getWordList(), parentid, con);
				      
			    con.close();
			    return true;
						
		} catch (SQLException ex) {
			System.out.println("doc Registration error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public ArrayList<String> getdynamicHeaderList(Connection con) throws SQLException
	{
		ArrayList<String> res = new ArrayList<>();
		res.add("word");
		ArrayList<DocumentData> docuList = getDocumentList(con);
		for (DocumentData documentData : docuList) {
			res.add(documentData.getName());
		}
		return res;
	}
	
	public List<HashMap<String, String>> getDynamicWordCountList(WordCountSearchData searchData,Connection con) throws SQLException
	{
		List<HashMap<String, String>> res = new ArrayList<>();		
		ArrayList<String> distinctWordList = new ArrayList<>();
		distinctWordList = getDistinctWordList(searchData , con);
		ArrayList<DocumentData>documentlist = new ArrayList<>();
		documentlist =getDocumentList(con);
		
		for (String word : distinctWordList) 
		{
			HashMap<String, String> wordCount = new HashMap<>();
			wordCount.put("word", word);
			for (DocumentData header : documentlist) {
				int count = getsimilarWordCountbyDocument(header.getId() , word ,con);
				wordCount.put(header.getName(), String.valueOf(count));
			}
			res.add(wordCount);
		}
		
		return res;
	}
	


	
	public  DocumentSearchPaginateData find(DocumentSearchData usearch)
	{
		DocumentSearchPaginateData res = new DocumentSearchPaginateData();
		String filter = getCriteria(usearch);
		List<DocumentData> resList = new ArrayList<DocumentData>();
		Connection con = null;
		DocumentData document;
		String query = "Select* from DOCUMENT "+filter;
			
		try {
			con = DataBaseConnection.getConnection();
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 document = new DocumentData();
				 document.setName(rs.getString("document_name"));
				 document.setCode(rs.getString("doc_code"));
				 document.setId(rs.getInt("document_id"));
				 document.setDescription(rs.getString("description"));
				 document.setAuthorName(rs.getString("author_name"));
				 resList.add(document);
			 }
			 	
			res.setCount(getTotalDocCount(usearch,con));
			res.setDocumentDataList(resList);
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
			
		return res;
	}
	
	public int getsimilarWordCountbyDocument(int parentid,String wordname,Connection con) throws SQLException
	{
		int res = 0 ;
		String query = "Select WORD_COUNT from WORD_LIST where doc_parent_id="+parentid+" and word_name like '"+wordname+"'";
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		while(rs.next())
		{
			res = rs.getInt("WORD_COUNT");
		}
		 
		return res;
	}
	
	public ArrayList<String> getDocumentColumnList() throws SQLException
	{
		ArrayList<String> wordList = new ArrayList<String>();
		Connection con = null;
		String query = "Select document_name from DOCUMENT order by document_id";
		con = DataBaseConnection.getConnection();
		Statement st = con.createStatement();
		ResultSet rs = st.executeQuery(query);
		wordList.add("Term"); 
		 while(rs.next())
		 {
			 
			 wordList.add(rs.getString("document_name"));
		 }
		
		return wordList;
	}
	
	public ArrayList<DocumentData> getDocumentList(Connection con) throws SQLException
	{
		ArrayList<DocumentData> wordList = new ArrayList<DocumentData>();
	
		String query = "Select * from DOCUMENT order by document_id";
		
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {
			 DocumentData documentData = new DocumentData();
			 documentData.setId(rs.getInt("DOCUMENT_ID"));
			 documentData.setAuthorName(rs.getString("AUTHOR_NAME"));
			 documentData.setName(rs.getString("DOCUMENT_NAME"));
			 documentData.setWords(getWordListbyparenId(documentData.getId(),con));
			 
			 wordList.add(documentData);
		 }
		
		return wordList;
	}
	
	public   boolean update(DocumentData doc) {
		Connection con = null;
		PreparedStatement ps = null;
		String query;
		
		try {
			con = DataBaseConnection.getConnection();
			     
			query = " update DOCUMENT set doc_code =? , author_name=? , document_name=? ,  description=?"
					+ ",filepath=? where document_id=?";
			    	        
			    int i = 1;
				ps = con.prepareStatement(query);
				ps.setString(i++, doc.getCode());
				ps.setString(i++, doc.getAuthorName());
				ps.setString(i++, doc.getName());
				ps.setString(i++, doc.getDescription());
				ps.setString(i++, doc.getFilepath());
				ps.setInt(i++, doc.getId());
				
				ps.execute();
				
				deleteWordList(doc.getId(), con);
				insertWordList(doc.getWordList(), doc.getId(), con);
				
			      
			    con.close();
			    return true;
			
			
			
		} catch (SQLException ex) {
			System.out.println("doc Registration error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public  int getWordPerDocCount(String word,Connection con)
	{
		int count = 0;

		String query = "Select count(*) as count from DOCUMENT";
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	
	
	
	public  int getTotalDocCount(DocumentSearchData search,Connection con)
	{
		int count = 0;
		String filter = getCountCriteria(search);
		String query = "Select count(*) as count from DOCUMENT "+ filter;
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public  int getTotalDocumentCount(Connection con)
	{
		int count = 0;
		String query = "Select count(*) as count from DOCUMENT ";
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public  int getWordCountByDoucument(String search,Connection con)
	{
		int count = 0;
		String query = "SELECT COUNT(*),DOC_PARENT_ID FROM `word_list` WHERE "
				+ "BINARY WORD_NAME = '"+search+"' GROUP BY DOC_PARENT_ID";
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			count = rs.getRow();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	
	
	public  boolean viladateDocID(String id)
	{
	
		boolean flag = true;
		Connection con = DataBaseConnection.getConnection();
		String query = "SELECT * FROM `document`"
				+ " where  DOC_CODE  = '"+ id+"'";
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
			{
				flag = false;
			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			DataBaseConnection.close(con);
		}
		 return flag;
	}
	
	public  double getSimilarityEqualWordWeight(ArrayList<WordData>wordList,int parentid)
	{
		DecimalFormat df = new DecimalFormat("#.00"); 
		Connection con = DataBaseConnection.getConnection();
		double totalSimilarityWeight = 0;
		double originalWeight = 0;
		double userWeight = 0;

		for (WordData word : wordList) {
			word.setWord(word.getWord().replaceAll("\\\\", ""));
			String query = "SELECT WEIGHT as weight FROM `word_list` WHERE "
					+ "BINARY WORD_NAME = '"+word.getWord()+"' AND DOC_PARENT_ID = "+parentid;
			Statement st;
			try 
			{
				st = con.createStatement();
				ResultSet rs = st.executeQuery(query);
				if(rs.next())
				{
					if(word.getWeight()>0)
					{
						originalWeight = Double.parseDouble(df.format(rs.getDouble("weight")));
						userWeight = Double.parseDouble(df.format(word.getWeight()));
						totalSimilarityWeight = Double.parseDouble(df.format( totalSimilarityWeight +( originalWeight * userWeight )));				
					}
					
				}
					
				
			}catch(NumberFormatException ex){ // handle your exception
				ex.printStackTrace();
			}
			catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		 return totalSimilarityWeight;
	}

	
	public  int getTotalWordCount(Connection con)
	{
		int count = 0;
		String query = "Select count(*) as count from word_list;";
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public  int getTotalWordCountByParentId(int parentid , Connection con)
	{
		int count = 0;
		String query = "Select count(*) as count from word_list where DOC_PARENT_ID = "+parentid + " and WEIGHT= 0 ";
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			if(rs.next())
				count = rs.getInt("count");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 return count;
	}
	
	public static String getCriteria(DocumentSearchData criteria)
	{
		String filter = "where 1=1";
		
		if(!criteria.getAuthorName().equals(""))			
		{
			filter += " and author_name like '%"+criteria.getAuthorName()+"%'";
			
		}
		if(!criteria.getDocCode().equals(""))
		{
			filter += " and doc_code="+criteria.getDocCode();
		}
		if(!criteria.getDocName().equals(""))
		{
			
			filter += " and document_name like '%"+criteria.getDocName()+"%'";
		}
	
		filter += " limit "+criteria.getOffset()+","+criteria.getLimit();
		
		return filter;
	}
	
	public static String getCountCriteria(DocumentSearchData criteria)
	{
		String filter = "where 1=1";
		
		if(!criteria.getAuthorName().equals(""))			
		{
			filter += " and author_name like '%"+criteria.getAuthorName()+"%'";
			
		}
		if(!criteria.getDocCode().equals(""))
		{
			filter += " and doc_code="+criteria.getDocCode();
		}
		if(!criteria.getDocName().equals(""))
		{
			
			filter += " and document_name like '%"+criteria.getDocName()+"%'";
		}
	
		
		
		return filter;
	}
	
	
	public int getMaxDocumentkey() 
	{
		Connection con = null;
		con = DataBaseConnection.getConnection();
		int res = 0;
		String query = "Select max(DOCUMENT_ID) from document";
		Statement st;
		try {
			st = con.createStatement();
			ResultSet rs = st.executeQuery(query);
			
			if(rs.next())
			{
				res = rs.getInt(1);
						
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return res;
	}
	
	public  DocumentData getDocumentByParentid(int id) throws SQLException
	{
		 DocumentData documentData = new DocumentData();
		Connection con = null;
		String query = "Select * from DOCUMENT where document_id = "+id;
		con = DataBaseConnection.getConnection();
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {			
			 documentData.setCode(rs.getString("doc_code"));
			 documentData.setId(rs.getInt("DOCUMENT_ID"));
			 documentData.setAuthorName(rs.getString("AUTHOR_NAME"));
			 documentData.setName(rs.getString("DOCUMENT_NAME"));
			 documentData.setDescription(rs.getString("description"));
			 documentData.setFilepath(rs.getString("filepath"));
			 documentData.setWords(getWordListbyparenId(documentData.getId(),con));
			 documentData.setWordList(getWordDataListbyparenId(id,con));
		 }
		
		return documentData;
	}
	
	public void addWordList(ArrayList<DocumentData> docList, Connection con) throws SQLException
	{
		for (DocumentData documentData : docList) 
		{
			documentData.setWords(getWordListbyparenId(documentData.getId(),con));
		}
	}
	
	public  ArrayList<String> getWordListbyparenId(int parentid,Connection con) throws SQLException
	{
		ArrayList<String> wordList = new ArrayList<String>();
		String query = "Select * from WORD_LIST where DOC_PARENT_ID = "+parentid;
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {
			 wordList.add( rs.getString("WORD_NAME"));
			 
		 }
		
		return wordList;
	}
	
	public  ArrayList<DocumentData> getAllData(Connection con)
	{

		ArrayList<DocumentData> resList = new ArrayList<DocumentData>();
		DocumentData document;
		String query = "Select* from DOCUMENT ";
		
		
		try {
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 document = new DocumentData();
				 document.setName(rs.getString("document_name"));
				 document.setCode(rs.getString("doc_code"));
				 document.setId(rs.getInt("document_id"));
				 document.setDescription(rs.getString("description"));
				 document.setAuthorName(rs.getString("author_name"));
				 document.setFilepath(rs.getString("filepath"));
				 document.setWordList(getWordDataListbyparenId(document.getId(),con));
				 resList.add(document);
			 }
			 
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return resList;
	}
	
	public  ArrayList<DocumentData> getAllDatawithProcessValue(Connection con)
	{

		ArrayList<DocumentData> resList = new ArrayList<DocumentData>();
		DocumentData document;
		String query = "Select* from DOCUMENT ";
		
		
		try {
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 ProcessValueDao processvalueDao = new ProcessValueDao();
			 
			 while(rs.next())
			 {
				 document = new DocumentData();
				 document.setName(rs.getString("document_name"));
				 document.setCode(rs.getString("doc_code"));
				 document.setId(rs.getInt("document_id"));
				 document.setDescription(rs.getString("description"));
				 document.setAuthorName(rs.getString("author_name"));
				 document.setFilepath(rs.getString("filepath"));
				 document.setWordList(getWordDataListbyparenId(document.getId(),con));
				 document.setProcessValuelsit(processvalueDao.getAllData(document.getId(),con));
				 resList.add(document);
			 }
			 
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return resList;
	}
	
	
	
	public WordPaginateData getWordDataListbyparenId(WordSearchData wordSearch) throws SQLException
	{
		WordPaginateData res = new WordPaginateData();
		ArrayList<WordData> wordList = new ArrayList<WordData>();
		Connection con = null;
		String query = "Select * from WORD_LIST where DOC_PARENT_ID = "+wordSearch.getId() +" and WEIGHT= 0 limit "+wordSearch.getOffset()+","+wordSearch.getLimit();
		con = DataBaseConnection.getConnection();
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {
			WordData word = new WordData();
			word.setCount(rs.getInt("word_count"));
			word.setWord(rs.getString("WORD_NAME"));
			word.setTf(rs.getDouble("tf"));
			word.setIdf(rs.getDouble("idf"));
			word.setWeight(rs.getDouble("weight"));
			 wordList.add(word);
		 }
		 res.setWordList(wordList);
		 res.setCount(getTotalWordCountByParentId(wordSearch.getId(), con));
		
		return res;
	}
	
	public ArrayList<WordData> getWordDataListbyparenId(int parentid , Connection con) throws SQLException
	{
		ArrayList<WordData> wordList = new ArrayList<WordData>();
		String query = "Select * from WORD_LIST where DOC_PARENT_ID = "+parentid;
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {
			WordData word = new WordData();
			word.setCount(rs.getInt("word_count"));
			word.setWord(rs.getString("WORD_NAME"));
			word.setTf(rs.getDouble("tf"));
			word.setIdf(rs.getDouble("idf"));
			word.setWeight(rs.getDouble("weight"));
			 wordList.add(word);
		 }
		
		return wordList;
	}
	
	public ArrayList<String> getDistinctWordList(WordCountSearchData searchData , Connection con) throws SQLException
	{
		ArrayList<String> wordList = new ArrayList<String>();
		String query = "Select distinct(word_name) from WORD_LIST order by WORD_NAME;";
		Statement st = con.createStatement();
		 ResultSet rs = st.executeQuery(query);
		 
		 while(rs.next())
		 {
			wordList.add(rs.getString("word_name"));
		 }
		
		return wordList;
	}
	
	public  boolean insertWordList(ArrayList<WordData> worList, int parentid , Connection con) throws SQLException
	{
		String query = "";
		PreparedStatement ps = null;
		 query = " insert into WORD_LIST (DOC_PARENT_ID, WORD_NAME, TF , IDF,WEIGHT,WORD_COUNT ) values (?, ?,? , ?,?,?)";
		for (WordData word : worList) {
			
				int i = 1;
				ps = con.prepareStatement(query);
				ps.setInt(i++, parentid);
				ps.setString(i++, word.getWord());
				ps.setDouble(i++, word.getTf());
				ps.setDouble(i++, word.getIdf());
				ps.setDouble(i++, word.getWeight());
				ps.setInt(i++, word.getCount());
			 ps.execute();
			
		}
		
		return true;
	}
	
	public  boolean insertWordList(ArrayList<WordData> worList, int parentid ) throws SQLException
	{
		Connection con = DataBaseConnection.getConnection();
		boolean flag = false;
		try
		{
		if(deleteWordList(parentid, con))
		{
			String query = "";
			PreparedStatement ps = null;
			 query = " insert into WORD_LIST (DOC_PARENT_ID, WORD_NAME, TF , IDF,WEIGHT,WORD_COUNT ) values (?, ?,? , ?,?,?)";
			for (WordData word : worList) 
			{
				
					int i = 1;
					ps = con.prepareStatement(query);
					ps.setInt(i++, parentid);
					ps.setString(i++, word.getWord());
					ps.setDouble(i++, word.getTf());
					ps.setDouble(i++, word.getIdf());
					ps.setDouble(i++, word.getWeight());
					ps.setInt(i++, word.getCount());
				 ps.execute();
				
			}
			flag = true;
		}
		
		}finally {
			DataBaseConnection.close(con);
		}
		
		return flag;
	}
	
	public  boolean deleteWordList(int parentid , Connection con) throws SQLException
	{
		String query = "";
		PreparedStatement ps = null;
		query = "Delete from word_list where doc_parent_id = ?";
					
		int i = 1;
		ps = con.prepareStatement(query);
		ps.setInt(i++, parentid);
							
		ps.execute();
		
		return true;
	}
	
	public  boolean deleteDocumet(int parentid) throws SQLException
	{
		String query = "";
		Connection con = null;
		con = DataBaseConnection.getConnection();
		PreparedStatement ps = null;
		query = "Delete from document where document_id = ?";
					
		int i = 1;
		ps = con.prepareStatement(query);
		ps.setInt(i++, parentid);
							
		ps.execute();
		
		deleteWordList(parentid, con);
		
		return true;
	}
	
	public boolean resetVectorValues() throws SQLException
	{
		boolean flag = false;
		
		Connection con = null;
		con = DataBaseConnection.getConnection();
		PreparedStatement ps = null;
		String resetQuery = "Truncate table PROCESS_VALUE";
		String querymatrixA = "Truncate table MATRIXA";
		String queryVmartix = "Truncate table V_MATRIX";
		String querySingularMatrix = "Truncate table SINGULAR_MATRIX";
		String queryMatrixu = "Truncate table U_MATRIX";
		
		ps = con.prepareStatement(querymatrixA);
		ps.execute();
		
		ps = con.prepareStatement(queryVmartix);
		ps.execute();
		
		ps = con.prepareStatement(querySingularMatrix);
		ps.execute();
		
		ps = con.prepareStatement(queryMatrixu);
		ps.execute();
		
		ps = con.prepareStatement(resetQuery);
		ps.execute();

		flag = true;
		
		return flag;
	}
	
	public boolean deleteALlData() throws SQLException
	{
		String querydocument = "";
		String queryword = "";
		Connection con = null;
		con = DataBaseConnection.getConnection();
		PreparedStatement ps = null;
		querydocument = "Truncate table DOCUMENT";
		queryword = "Truncate table WORD_LIST";
		String querymatrixA = "Truncate table MATRIXA";
		String queryVmartix = "Truncate table V_MATRIX";
		String querySingularMatrix = "Truncate table SINGULAR_MATRIX";
		String queryMatrixu = "Truncate table U_MATRIX";
		String queryProcessValue = "Truncate table PROCESS_VALUE";
		
					
		ps = con.prepareStatement(querydocument);
							
		ps.execute();
		
		ps = con.prepareStatement(queryword);
		ps.execute();
		
		ps = con.prepareStatement(querymatrixA);
		ps.execute();
		
		ps = con.prepareStatement(queryVmartix);
		ps.execute();
		
		ps = con.prepareStatement(querySingularMatrix);
		ps.execute();
		
		ps = con.prepareStatement(queryMatrixu);
		ps.execute();
		
		ps = con.prepareStatement(queryProcessValue);
		ps.execute();
		
		
		
		
		return true;
	}
	

}
