package com.tfidf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.database.util.DataBaseConnection;
import com.tfidf.model.MatrixAData;
import com.tfidf.model.SVUMatrixData;

public class MatrixDao {
	
	public  boolean insertMatrixA(ArrayList<MatrixAData> matrixAList , Connection con) {
	
		PreparedStatement ps = null;
		String query;
		
		try {
			
			for (MatrixAData matrixA : matrixAList) {
				query = " insert into MATRIXA (WORD,COLUMN_DATA) values (?,?)";
    	        
			    int i = 1;
				ps = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
				ps.setString(i++, matrixA.getWordname());
				ps.setString(i++, matrixA.getColumndata());
				ps.execute();
			   
			}
			 con.close();	
			    return true;
				
		} catch (SQLException ex) {
			System.out.println("doc Registration error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	
	public  boolean insertSVUMatrix(ArrayList<SVUMatrixData> matrixList, String tablename, Connection con) {
		PreparedStatement ps = null;
		String query;
		
		try {
			
			for (SVUMatrixData matrixA : matrixList) {
				query = "Insert into "+tablename+"(COLUMN_DATA) values (?)";
    	        
			    int i = 1;
				ps = con.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
				ps.setString(i++, matrixA.getColumndata());
				ps.execute();
			   
			}
			 con.close();	
			    return true;
				
		} catch (SQLException ex) {
			System.out.println("doc Registration error -->" + ex.getMessage());
			return false;
		} finally {
			DataBaseConnection.close(con);
		}
	}
	

}
