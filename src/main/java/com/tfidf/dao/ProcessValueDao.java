package com.tfidf.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.tfidf.model.ProcessValue;

public class ProcessValueDao {
	
	public  boolean insertprocessList(ArrayList<ProcessValue> processvaluelsit, int parentid,Connection con) 
	{
		String query = "";
		try {
		PreparedStatement ps = null;
		 query = " insert into PROCESS_VALUE (PARENT_ID, PROCESS_VALUE ) values (?,?)";
		for (ProcessValue processvalue : processvaluelsit) {
			
				int i = 1;
				ps = con.prepareStatement(query);
				ps.setInt(i++, parentid);
				ps.setDouble(i++, processvalue.getProcess_value());
			 ps.execute();
			
		}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	
	public  ArrayList<ProcessValue> getAllData(int parentid, Connection con)
	{

		ArrayList<ProcessValue> resList = new ArrayList<ProcessValue>();
		ProcessValue processvalue;
		String query = "Select * from PROCESS_VALUE where parent_id="+parentid;
		
		
		try {
			Statement st = con.createStatement();
			 ResultSet rs = st.executeQuery(query);
			 
			 while(rs.next())
			 {
				 processvalue = new ProcessValue();
				 processvalue.setId(rs.getInt("ID"));
				 processvalue.setParent_id(rs.getInt("parent_id"));
				 processvalue.setProcess_value(rs.getDouble("process_value"));
				 resList.add(processvalue);
			 }
			 
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		return resList;
	}
	
	

}
