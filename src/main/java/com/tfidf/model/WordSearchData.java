package com.tfidf.model;

import java.io.Serializable;

public class WordSearchData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2919621885728350891L;
	
	int offset;
	int limit;
	int id;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}

	public WordSearchData()
	{
		this.id = 0;
		this.limit = 0;
		this.offset = 0;
	}
	
}
