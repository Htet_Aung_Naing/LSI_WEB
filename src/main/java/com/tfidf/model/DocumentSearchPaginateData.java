package com.tfidf.model;

import java.io.Serializable;
import java.util.List;

public class DocumentSearchPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<DocumentData> documentDataList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<DocumentData> getDocumentDataList() {
		return documentDataList;
	}
	public void setDocumentDataList(List<DocumentData> DocumentDataList) {
		this.documentDataList = DocumentDataList;
	}

}
