package com.tfidf.model;

import java.io.Serializable;
import java.util.List;

public class SimilarityWordCountPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<DocumentMatrixData> similarWordList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<DocumentMatrixData> getSimilarWordList() {
		return similarWordList;
	}
	public void setSimilarWordList(List<DocumentMatrixData> similarWordList) {
		this.similarWordList = similarWordList;
	}

}
