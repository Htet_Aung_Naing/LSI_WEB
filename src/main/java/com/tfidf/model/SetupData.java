package com.tfidf.model;

import java.io.Serializable;

public class SetupData implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5810356078043706364L;
	/**
	 * 
	 */
	
	String label;
	int value;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public int getValue() {
		return value;
	}
	public void setValue(int value) {
		this.value = value;
	}
	
	
	

}
