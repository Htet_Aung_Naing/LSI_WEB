package com.tfidf.model;

import java.io.Serializable;
import java.util.ArrayList;

import org.springframework.stereotype.Repository;

@Repository
public class DocumentMatrixData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2919621885728350891L;
	
	String word;
	
	int wordcount;
	int offset;
	int limit;
	String keyDoc;
	
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}

	
	public String getWord() {
		return word;
	}
	public void setWord(String word) {
		this.word = word;
	}
		public int getWordcount() {
		return wordcount;
	}
	public void setWordcount(int wordcount) {
		this.wordcount = wordcount;
	}
	public String getKeyDoc() {
		return keyDoc;
	}
	public void setKeyDoc(String keyDoc) {
		this.keyDoc = keyDoc;
	}
	public DocumentMatrixData()
	{
		this.word = "";
		this.wordcount = 0;
		this.limit = 0;
		this.offset = 0;
		this.keyDoc = "";
	}
	
}
