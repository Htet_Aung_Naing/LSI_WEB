package com.tfidf.model;

public class DynamicColumnData {
	
	String header;
	String property;
	public String getHeader() {
		return header;
	}
	public void setHeader(String header) {
		this.header = header;
	}
	public String getProperty() {
		return property;
	}
	public void setProperty(String property) {
		this.property = property;
	}
	
	public DynamicColumnData()
	{
		this.header = "";
		this.property = "";
	}

}
