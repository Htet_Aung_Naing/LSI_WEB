package com.tfidf.model;

import java.util.ArrayList;

public class MatrixAData {
	String wordname;
	String columndata;
	ArrayList<String> columnlist;
	String processdata;
	
	public String getProcessdata() {
		return processdata;
	}
	public void setProcessdata(String processdata) {
		this.processdata = processdata;
	}
	public String getWordname() {
		return wordname;
	}
	public void setWordname(String wordname) {
		this.wordname = wordname;
	}
	public String getColumndata() {
		return columndata;
	}
	public void setColumndata(String columndata) {
		this.columndata = columndata;
	}
	
	public ArrayList<String> getColumnlist() {
		return columnlist;
	}
	public void setColumnlist(ArrayList<String> columnlist) {
		this.columnlist = columnlist;
	}
	public MatrixAData()
	{
		this.wordname = "";
		this.columndata = "";
		this.columnlist = new ArrayList<>();
		this.processdata = "";
	}

}
