package com.tfidf.model;

import java.util.ArrayList;

public class MatrixData {
	
	ArrayList<Double> columnlist;

	public ArrayList<Double> getColumnlist() {
		return columnlist;
	}

	public void setColumnlist(ArrayList<Double> columnlist) {
		this.columnlist = columnlist;
	}
	
	public MatrixData()
	{
		this.columnlist = new ArrayList<>();
	}
			

}
