package com.tfidf.model;

import java.io.Serializable;

public class WordCountSearchData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2919621885728350891L;
	
	String docCode;
	String docName;
	String authorName;
	int offset;
	int limit;
	
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getDocCode() {
		return docCode;
	}
	public void setDocCode(String docCode) {
		this.docCode = docCode;
	}
	public String getDocName() {
		return docName;
	}
	public void setDocName(String docName) {
		this.docName = docName;
	}
	public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	public WordCountSearchData()
	{
		this.authorName = "";
		this.docName = "";
		this.docCode = "";
		this.authorName = "";
		this.limit = 0;
		this.offset = 0;
	}
	
}
