package com.tfidf.model;

import java.io.Serializable;
import java.util.List;

public class WordPaginateData implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8793922769046576099L;
	
	int count;
	List<WordData> wordList;
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public List<WordData> getWordList() {
		return wordList;
	}
	public void setWordList(List<WordData> wordList) {
		this.wordList = wordList;
	}

}
