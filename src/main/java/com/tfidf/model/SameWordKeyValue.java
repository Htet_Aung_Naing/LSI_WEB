package com.tfidf.model;

import java.io.Serializable;
import java.util.ArrayList;

public class SameWordKeyValue implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -921438821809402237L;
	
	String wordKey;
	ArrayList<DocumentMatrixData> valueMatrixList;
	public String getWordKey() {
		return wordKey;
	}
	public void setWordKey(String wordKey) {
		this.wordKey = wordKey;
	}
	public ArrayList<DocumentMatrixData> getValueMatrixList() {
		return valueMatrixList;
	}
	public void setValueMatrixList(ArrayList<DocumentMatrixData> valueMatrixList) {
		this.valueMatrixList = valueMatrixList;
	}
	
	public SameWordKeyValue()
	{
		this.wordKey = "";
		this.valueMatrixList = new ArrayList<>();
	}

}
