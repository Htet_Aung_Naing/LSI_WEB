package com.tfidf.model;

import java.io.Serializable;
import java.util.Comparator;

import org.primefaces.model.StreamedContent;

public class ComparisonPaperData implements Serializable{
	
	/**
	 * 
	 */
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4929214282530914970L;
	/**
	 * 
	 */
	
	String label;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public double similarityValue;
	String code;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public double getSimilarityValue() {
		return similarityValue;
	}
	public void setSimilarityValue(double similarityValue) {
		this.similarityValue = similarityValue;
	}
	
	String authorName;
	
	 public String getAuthorName() {
		return authorName;
	}
	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}
	
	private StreamedContent downLoadFile;
	
	
	public StreamedContent getDownLoadFile() {
		return downLoadFile;
	}
	public void setDownLoadFile(StreamedContent downLoadFile) {
		this.downLoadFile = downLoadFile;
	}
	public static Comparator<ComparisonPaperData> DocumentSimilarityRank = new Comparator<ComparisonPaperData>()
			{

			public int compare(ComparisonPaperData s1, ComparisonPaperData s2) {

			   double rollno1 = s1.getSimilarityValue();
			   double rollno2 = s2.getSimilarityValue();

			   /*For ascending order*/
			   return (int) (rollno2-rollno1);

			   /*For descending order*/
			   //rollno2-rollno1;
		   }};
	
}
