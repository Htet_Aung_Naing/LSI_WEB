package com.tfidf.model;

public class ProcessValue {
	
	int parent_id;
	int id;
	double process_value;
	public int getParent_id() {
		return parent_id;
	}
	public void setParent_id(int parent_id) {
		this.parent_id = parent_id;
	}
	public double getProcess_value() {
		return process_value;
	}
	public void setProcess_value(double process_value) {
		this.process_value = process_value;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public ProcessValue()
	{
		this.id = 0;
		this.process_value = 0 ;
		this.parent_id = 0;
	}

}
