package com.database.util;

import java.util.Comparator;
import com.tfidf.model.ComparisonPaperData;

public class ComperatorClass implements Comparator<ComparisonPaperData>{

	@Override
	public int compare(ComparisonPaperData arg0, ComparisonPaperData arg1) {
		if (arg1.getSimilarityValue() < arg0.getSimilarityValue()) return -1;
        if (arg1.getSimilarityValue() > arg0.getSimilarityValue()) return 1;
        return 0;
	}

}
